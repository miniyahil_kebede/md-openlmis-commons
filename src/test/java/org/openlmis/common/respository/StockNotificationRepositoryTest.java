/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.respository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.openlmis.common.domain.StockNotification;
import org.openlmis.common.domain.StockNotificationDataBuilder;
import org.openlmis.common.dto.StockNotificationDto;
import org.openlmis.common.repository.StockNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
@SpringBootTest
@RunWith(SpringRunner.class)
public class StockNotificationRepositoryTest {

  private UUID requisitionId = UUID.fromString("b017b3b3-3bbd-47c5-a295-ce871098656e");

  private String elmisOrderNumber = "Test Order number";

  @Autowired
  private StockNotificationRepository stockNotificationRepository;

  private StockNotification notification;

  private StockNotificationDto notificationDto;

  private UUID notificationId;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    notification = new StockNotificationDataBuilder().build();
    notificationId = notification.getId();

    notificationDto = StockNotificationDto.newInstance(notification);

  }

  @Test
  public void shouldGetByRequisitionId() {

    UUID requisitionId2 = UUID.randomUUID();

    StockNotification notification2 =
        new StockNotificationDataBuilder().build();

    notification2.setRequisitionId(requisitionId2);

    stockNotificationRepository.save(notification2);

    StockNotification stockNotificationDto = stockNotificationRepository
        .findByRequisitionId(notification2.getRequisitionId());

    assertThat(notification2)
        .isEqualTo(stockNotificationDto);

  }

  @Test
  public void shouldGetByElmisOrderNumber() {

    String orderNumber2 = "some-order";

    StockNotification notification2 =
        new StockNotificationDataBuilder().build();

    notification2.setElmisOrderNumber(orderNumber2);

    stockNotificationRepository.save(notification2);

    StockNotification stockNotificationDto = stockNotificationRepository
        .findByElmisOrderNumber(notification2.getElmisOrderNumber());

    assertThat(notification2)
        .isEqualTo(stockNotificationDto);

  }

}

