/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.common.domain.MsdDailyStockStatus;
import org.openlmis.common.domain.MsdDailyStockStatusValue;
import org.openlmis.common.dto.MsdDailyStockStatusDto;
import org.openlmis.common.dto.MsdDailyStockStatusValueDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.service.MsdDailyStockStatusService;
import org.openlmis.common.util.MsdDailyStockStatusDataBuilder;
import org.openlmis.common.util.Pagination;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
public class MsdDailyStockStatusControllerTest {

  private static final String TEST_PLANT = "Plant 1";

  @Mock
  private MsdDailyStockStatusService stockStatusService;

  @InjectMocks
  private MsdDailyStockStatusController stockStatusController;

  private MsdDailyStockStatus stockStatus;

  private MsdDailyStockStatusDto stockStatusDto;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    stockStatus = new MsdDailyStockStatusDataBuilder().build();
    stockStatusDto = MsdDailyStockStatusDto.newInstance(stockStatus);
  }

  @Test
  public void shouldCreateMsdDailyStockStatus() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(stockStatusService.create(stockStatusDto))
        .thenReturn(stockStatus);

    MsdDailyStockStatusDto savedStockStatusDto = stockStatusController.create(
        stockStatusDto,
        bindingResult
    );
    verify(stockStatusService).create(stockStatusDto);
    Assertions.assertThat(savedStockStatusDto).isEqualTo(stockStatusDto);
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldThrowExceptionWhenValidationFails() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(bindingResult.hasErrors()).thenReturn(true);
    when(stockStatusService.create(stockStatusDto))
        .thenReturn(stockStatus);
    stockStatusController.create(
        stockStatusDto,
        bindingResult
    );
  }

  @Test
  public void shouldSearchMsdDailyStockStatuses() {
    List<MsdDailyStockStatusValue> expectedStockStatusValues =
        new LinkedList<>(stockStatus.getValues());

    when(stockStatusService.searchStockStatusValue(
        any(String.class),
        eq(pageable))
    ).thenReturn(Pagination.getPage(expectedStockStatusValues, pageable));

    Page<MsdDailyStockStatusValueDto> foundStockStatusValueDtos =
        stockStatusController.searchStockStatusValues(Optional.of(TEST_PLANT), pageable);

    verify(stockStatusService).searchStockStatusValue(
        any(String.class),
        eq(pageable)
    );

    List<MsdDailyStockStatusValueDto> expectedStockStatusValueDtos =
        new LinkedList<>(stockStatusDto.getValues());
    Assertions.assertThat(foundStockStatusValueDtos.getContent())
        .isEqualTo(expectedStockStatusValueDtos);
  }

}
