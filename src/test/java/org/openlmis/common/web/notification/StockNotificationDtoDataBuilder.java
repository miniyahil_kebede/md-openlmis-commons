/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web.notification;

import java.util.Collections;
import java.util.List;
import org.openlmis.common.domain.StockNotificationDataBuilder;
import org.openlmis.common.domain.StockNotificationLineItemDataBuilder;
import org.openlmis.common.dto.StockNotificationDto;
import org.openlmis.common.dto.StockNotificationLineItemDto;

public class StockNotificationDtoDataBuilder {

  private StockNotificationDto stockNotificationDto;

  /**
   * Constructs new notification dto data builder.
   */
  public StockNotificationDtoDataBuilder() {

    stockNotificationDto = new StockNotificationDto();
    new StockNotificationDataBuilder().build().export(stockNotificationDto);

    StockNotificationLineItemDto notificationLineItemDto
        = new StockNotificationLineItemDataBuilder().buildAsDto();
    stockNotificationDto.setLineItems(Collections.singletonList(notificationLineItemDto));

  }

  public StockNotificationDtoDataBuilder
      withLineItems(List<StockNotificationLineItemDto> lineItems) {
    stockNotificationDto.setLineItems(lineItems);
    return this;
  }

  public StockNotificationDtoDataBuilder withoutQuoteNumber() {
    stockNotificationDto.setQuoteNumber(null);
    return this;
  }

  public StockNotificationDtoDataBuilder withoutCustomerName() {
    stockNotificationDto.setCustomerName(null);
    return this;
  }

  public StockNotificationDtoDataBuilder withoutCustomerId() {
    stockNotificationDto.setCustomerId(null);
    return this;
  }

  public StockNotificationDtoDataBuilder withoutElmisOrderNumber() {
    stockNotificationDto.setElmisOrderNumber(null);
    return this;
  }

  public StockNotificationDtoDataBuilder withoutNotificationDate() {
    stockNotificationDto.setNotificationDate(null);
    return this;
  }

  public StockNotificationDtoDataBuilder withoutProcessingDate() {
    stockNotificationDto.setProcessingDate(null);
    return this;
  }

  public StockNotificationDtoDataBuilder withoutZone() {
    stockNotificationDto.setZone(null);
    return this;
  }

  public StockNotificationDtoDataBuilder withoutComment() {
    stockNotificationDto.setComment(null);
    return this;
  }

  /**
   * Builds instance of {@link StockNotificationLineItemDto}.
   */
  public StockNotificationDto build() {
    return stockNotificationDto;
  }

}
