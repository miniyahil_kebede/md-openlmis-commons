/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web.notification;

import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openlmis.common.domain.StockNotification;
import org.openlmis.common.dto.StockNotificationDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.MessageKeys;
import org.openlmis.common.repository.StockNotificationRepository;
import org.openlmis.common.service.fulfillment.OrderFulfillmentService;
import org.openlmis.common.web.StockNotificationController;

public class StockNotificationControllerTest {

  @Rule
  public final ExpectedException exception = ExpectedException.none();

  @Mock
  private StockNotificationRepository stockNotificationRepository;

  @InjectMocks
  private OrderFulfillmentService orderFulfillmentService;

  @InjectMocks
  private StockNotificationController stockNotificationController
      = new StockNotificationController();

  private StockNotificationDto stockNotificationDto
      = new StockNotificationDtoDataBuilder().build();

  private StockNotification stockNotification
      = StockNotification.newInstance(stockNotificationDto);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    when(stockNotificationRepository.save(any(StockNotification.class)))
        .thenReturn(stockNotification);

  }

  @Test
  public void shouldThrowExceptionIfRequisitionIdIsNotProvided() {
    exception.expect(ValidationMessageException.class);
    exception.expectMessage(MessageKeys.MUST_CONTAIN_VALUE);
    stockNotificationDto.setRequisitionId(null);
    stockNotificationController.updateNotification(stockNotificationDto);
  }

  @Test(expected = NullPointerException.class)
  public void throwNullExceptionIfOrderIsNotFound() {

    stockNotificationController.updateNotification(stockNotificationDto);

  }

  @Test(expected = NullPointerException.class)
  public void cannotProcessNotificationWhenOrderIdIsNotFound() {

    when(orderFulfillmentService.findOne(stockNotificationDto.getRequisitionId()))
        .thenReturn(null);
    exception.expect(ValidationMessageException.class);
    exception.expect(hasProperty("params",
        arrayContaining(stockNotificationDto.getRequisitionId().toString())));
    exception.expectMessage(MessageKeys.NOT_FOUND);

  }

}
