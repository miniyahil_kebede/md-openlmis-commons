/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.util.UUID;
import javax.persistence.MappedSuperclass;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@MappedSuperclass
@EqualsAndHashCode
@ToString
public abstract class StockLineItemBaseTest {

  public UUID id = UUID.fromString("15498136-8972-4ade-9f14-602bf199652b");

  public String itemCode = "Item-code-123";

  public String itemDescription = "item-Description-123";
  public String uom = "1Tu";
  public Integer quantity = 1500;
  public Integer quantityShipped = 1246;
  public Integer quantityOrdered = 8102;
  public String missingItemStatus = "OUT-OF-STOCK";
  public String dueDate = "2018-10-25";


  public StockLineItemBaseTest withId(UUID id) {
    this.id = id;
    return this;
  }

  public StockLineItemBaseTest withItemCode(String itemCode) {
    this.itemCode = itemCode;
    return this;
  }

  public StockLineItemBaseTest withItemDescription(String itemDescription) {
    this.itemDescription = itemDescription;
    return this;
  }

  public StockLineItemBaseTest withUom(String uom) {
    this.uom = uom;
    return this;
  }

  public StockLineItemBaseTest withQuantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  public StockLineItemBaseTest withQuantityShipped(Integer quantityShipped) {
    this.quantityShipped = quantityShipped;
    return this;
  }

  public StockLineItemBaseTest withQuantityOrdered(Integer quantityOrdered) {
    this.quantityOrdered = quantityOrdered;
    return this;
  }

  public StockLineItemBaseTest withMissingItemStatus(String missingItemStatus) {
    this.missingItemStatus = missingItemStatus;
    return this;
  }

  public StockLineItemBaseTest withDueDate(String dueDate) {
    this.dueDate = dueDate;
    return this;
  }

  public abstract StockNotificationLineItem build();
}
