/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common;

import static junit.framework.TestCase.assertEquals;

import java.io.IOException;
import java.util.List;
import org.javers.core.Javers;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.repository.jql.QueryBuilder;
import org.joda.time.DateTimeZone;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openlmis.common.dto.JasperTemplateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
@Transactional
public class JaVersIntegrationTest {

  private static DateTimeZone defaultZone;

  @Autowired
  private Javers javers;

  @BeforeClass
  public static void beforeClass() {
    defaultZone = DateTimeZone.getDefault();
  }

  @After
  public void after() {
    DateTimeZone.setDefault(defaultZone);
  }

  @Test
  public void shouldAlwaysCommitWithUtcTimeZone() throws IOException {

    //given
    JasperTemplateDto dto = new JasperTemplateDto();
    dto.setName("note 1");

    //when
    DateTimeZone.setDefault(DateTimeZone.forID("UTC"));
    //javers.commit(COMMIT_AUTHOR, dto);

    DateTimeZone.setDefault(DateTimeZone.forID("Africa/Johannesburg"));
    dto.setName("note 2");
    //javers.commit(COMMIT_AUTHOR, dto);

    //then
    List<CdoSnapshot> snapshots = javers.findSnapshots(
        QueryBuilder.byClass(JasperTemplateDto.class).build());
    assertEquals(0, snapshots.size());

  }

}