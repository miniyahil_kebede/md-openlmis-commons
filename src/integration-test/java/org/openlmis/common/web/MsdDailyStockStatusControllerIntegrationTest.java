/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.openlmis.common.web.MsdDailyStockStatusController.RESOURCE_PATH;

import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.LinkedList;
import java.util.List;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.common.domain.MsdDailyStockStatus;
import org.openlmis.common.domain.MsdDailyStockStatusValue;
import org.openlmis.common.dto.MsdDailyStockStatusDto;
import org.openlmis.common.util.MsdDailyStockStatusDataBuilder;
import org.openlmis.common.util.Pagination;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class MsdDailyStockStatusControllerIntegrationTest extends BaseWebIntegrationTest {

  private static final String PARAM_NAME = "plant";

  private MsdDailyStockStatus stockStatus;

  private MsdDailyStockStatusDto stockStatusDto;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    mockUserAuthenticated();

    stockStatus = new MsdDailyStockStatusDataBuilder().build();
    stockStatusDto = MsdDailyStockStatusDto.newInstance(stockStatus);
  }

  @Test
  public void shouldPostMsdDailyStockStatusDto() {
    when(stockStatusService.create(eq(stockStatusDto))).thenReturn(stockStatus);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(stockStatusDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract().as(MsdDailyStockStatusDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPostRequest() {
    MsdDailyStockStatusDto requestBody = new MsdDailyStockStatusDto();

    when(stockStatusService.create(any(MsdDailyStockStatus.Importer.class)))
        .thenReturn(stockStatus);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(requestBody)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }

  @Test
  public void shouldGetAllMsdDailyStockStatuses() {
    List<MsdDailyStockStatusValue> expectedStockStatusValues =
        new LinkedList<>(stockStatus.getValues());
    Page<MsdDailyStockStatusValue> expectedStockStatusValuePage = Pagination.getPage(
        expectedStockStatusValues, pageable
    );

    given(stockStatusService.searchStockStatusValue(any(String.class), any(Pageable.class)))
        .willReturn(expectedStockStatusValuePage);

    given(stockStatusService.findAllStockStatusValue(any(Pageable.class)))
        .willReturn(expectedStockStatusValuePage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedStockStatusValues.size()))
        .body(CONTENT_ID, hasSize(expectedStockStatusValues.size()))
        .body(CONTENT_ID, hasItem(expectedStockStatusValues.get(0).getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldSearchMsdDailyStockStatuses() {
    List<MsdDailyStockStatusValue> expectedStockStatusValues =
        new LinkedList<>(stockStatus.getValues());
    Page<MsdDailyStockStatusValue> expectedStockStatusValuePage = Pagination.getPage(
        expectedStockStatusValues, pageable
    );

    String plantFilter = expectedStockStatusValues.get(0).getPlant();

    given(stockStatusService.searchStockStatusValue(any(String.class), any(Pageable.class)))
        .willReturn(expectedStockStatusValuePage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .queryParam(PARAM_NAME, plantFilter)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedStockStatusValues.size()))
        .body(CONTENT_ID, hasSize(expectedStockStatusValues.size()))
        .body(CONTENT_ID, hasItem(expectedStockStatusValues.get(0).getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

}
