/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.openlmis.common.dto.StockNotificationLineItemDto;

@Entity
@Table(name = "stock_notification_line_items")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StockNotificationLineItem extends BaseEntity {

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private String itemCode;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private String itemDescription;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private String uom;

  @Column(columnDefinition = INTEGER_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private Integer quantity;

  @Column(columnDefinition = INTEGER_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private Integer quantityShipped;

  @Column(columnDefinition = INTEGER_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private Integer quantityOrdered;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private String missingItemStatus;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private String dueDate;

  @ManyToOne(cascade = CascadeType.REFRESH)
  @JoinColumn(name = "notificationId", nullable = false)
  @Getter
  @Setter
  private StockNotification notification;

  /**
   * Creates a new instance of stock notification line item.
   *
   * @param itemCode          itemCode parameter.
   * @param itemDescription   itemDescription for stock out notification line item.
   * @param uom               uom for stock out notification line item.
   * @param quantity          quantity for stock out notification line item.
   * @param quantityShipped   quantityShipped for stock out notification line item.
   * @param quantityOrdered   quantityOrdered for stock out notification line item.
   * @param missingItemStatus missingItemStatus for stock out notification line item.
   * @param dueDate           dueDate for stock out notification line item.
   */
  public StockNotificationLineItem(String itemCode,
                                   String itemDescription,
                                   String uom,
                                   Integer quantity,
                                   Integer quantityShipped,
                                   Integer quantityOrdered,
                                   String missingItemStatus,
                                   String dueDate) {
    this.itemCode = itemCode;
    this.itemDescription = itemDescription;
    this.uom = uom;
    this.quantity = quantity;
    this.quantityShipped = quantityShipped;
    this.quantityOrdered = quantityOrdered;
    this.missingItemStatus = missingItemStatus;
    this.dueDate = dueDate;

  }

  /**
   * Create new instance of StockNotificationLineItem based on given.
   * {@link StockNotificationLineItem.Importer}.
   *
   * @param importer instance of {@link StockNotificationLineItem.Importer}.
   *
   * @return instance of StockNotificationLineItem.
   */
  public static StockNotificationLineItem newInstance(Importer importer) {

    StockNotificationLineItem lineItem = new StockNotificationLineItem();
    lineItem.setId(importer.getId());
    lineItem.setItemCode(importer.getItemCode());
    lineItem.setItemDescription(importer.getItemDescription());
    lineItem.setUom(importer.getUom());
    lineItem.setQuantity(importer.getQuantity());
    lineItem.setQuantityShipped(importer.getQuantityShipped());
    lineItem.setQuantityOrdered(importer.getQuantityOrdered());
    lineItem.setMissingItemStatus(importer.getMissingItemStatus());
    lineItem.setDueDate(importer.getDueDate());
    return lineItem;

  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */

  public void export(StockNotificationLineItemDto exporter) {
    exporter.setId(id);
    exporter.setItemCode(itemCode);
    exporter.setItemDescription(itemDescription);
    exporter.setUom(uom);
    exporter.setQuantity(quantity);
    exporter.setQuantityShipped(quantityShipped);
    exporter.setQuantityOrdered(quantityOrdered);
    exporter.setMissingItemStatus(missingItemStatus);
    exporter.setDueDate(dueDate);
  }

  public interface Exporter {
    void setId(UUID id);

    void setItemCode(String itemCode);

    void setItemDescription(String itemDescription);

    void setUom(String uom);

    void setQuantity(Integer quantity);

    void setQuantityShipped(Integer quantityShipped);

    void setQuantityOrdered(Integer quantityOrdered);

    void setMissingItemStatus(String missingItemStatus);

    void setDueDate(String dueDate);
  }

  public interface Importer {
    UUID getId();

    String getItemCode();

    String getItemDescription();

    String getUom();

    Integer getQuantity();

    Integer getQuantityShipped();

    Integer getQuantityOrdered();

    String getMissingItemStatus();

    String getDueDate();
  }

  /**
   * Exports data from the given lineItem to the instance that implement {@link Exporter}
   * interface.
   */
  public void export(Exporter exporter) {
    exporter.setId(getId());
  }

}
