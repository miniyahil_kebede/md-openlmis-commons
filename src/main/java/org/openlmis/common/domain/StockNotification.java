/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import static org.apache.commons.collections.CollectionUtils.isEmpty;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.openlmis.common.dto.StockNotificationDto;
import org.openlmis.common.dto.StockNotificationLineItemDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.MessageKeys;

@Entity
@Table(name = "stock_notifications")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StockNotification extends BaseEntity {

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION)
  @Getter
  @Setter
  private String quoteNumber;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private UUID requisitionId;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, nullable = false)
  @Getter
  @Setter
  private String customerId;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION)
  @Getter
  @Setter
  private String customerName;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION)
  @Getter
  @Setter
  private String hfrCode;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, unique = true, nullable = false)
  @Getter
  @Setter
  private String elmisOrderNumber;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, nullable = false)
  @Getter
  @Setter
  private String notificationDate;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, nullable = false)
  @Getter
  @Setter
  private String processingDate;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION, nullable = false)
  @Getter
  @Setter
  private String zone;

  @Column(columnDefinition = TEXT_COLUMN_DEFINITION)
  @Getter
  @Setter
  private String comment;

  @OneToMany(
      mappedBy = "notification",
      cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,
          CascadeType.REMOVE},
      fetch = FetchType.EAGER,
      orphanRemoval = true)
  @Fetch(FetchMode.SELECT)
  //@JsonProperty("stockOutItems")
  @Getter
  @Setter
  private List<StockNotificationLineItem> lineItems;

  @PrePersist
  @PreUpdate
  private void preSave() {
    if (lineItems != null) {
      lineItems.forEach(lineItem -> lineItem.setNotification(this));
    }
  }

  /**
   * Creates a new instance of stock notification.
   *
   * @param id               id parameter.
   * @param quoteNumber      quoteNumber property to extract.
   * @param customerId       customerId parameter.
   * @param hfrCode          hfrCode parameter.
   * @param elmisOrderNumber elmisOrderNumber parameter.
   * @param notificationDate notificationDate parameter.
   * @param processingDate   processingDate parameter.
   * @param zone             zone parameter.
   * @param comment          comment parameter.
   */
  public StockNotification(UUID id, String quoteNumber,
                           String customerId, String customerName, String hfrCode,
                           String elmisOrderNumber, String notificationDate,
                           String processingDate, String zone,
                           String comment,
                           UUID requisitionId) {
    this.id = id;
    this.quoteNumber = quoteNumber;
    this.customerId = customerId;
    this.customerName = customerName;
    this.hfrCode = hfrCode;
    this.elmisOrderNumber = elmisOrderNumber;
    this.notificationDate = notificationDate;
    this.processingDate = processingDate;
    this.zone = zone;
    this.comment = comment;
    this.requisitionId = requisitionId;
  }

  /**
   * Create new instance of notification based on given.
   * {@link StockNotification.Importer}.
   *
   * @param importer instance of {@link StockNotification.Importer}.
   *
   * @return instance of StockNotification.
   */
  public static StockNotification newInstance(Importer importer) {

    validateLineItems(importer.getLineItems());

    List<StockNotificationLineItem> items = new ArrayList<>(importer.getLineItems().size());
    if (importer.getLineItems() != null) {
      importer.getLineItems().stream()
          .map(StockNotificationLineItem::newInstance)
          .forEach(items::add);
    }
    StockNotification notification = new StockNotification();
    notification.setId(importer.getId());
    notification.setQuoteNumber(importer.getQuoteNumber());
    notification.setCustomerId(importer.getCustomerId());
    notification.setCustomerName(importer.getCustomerName());
    notification.setHfrCode(importer.getHfrCode());
    notification.setElmisOrderNumber(importer.getElmisOrderNumber());
    notification.setNotificationDate(importer.getNotificationDate());
    notification.setProcessingDate(importer.getProcessingDate());
    notification.setZone(importer.getZone());
    notification.setComment(importer.getComment());
    notification.setRequisitionId(importer.getRequisitionId());
    notification.setLineItems(items);

    return notification;
  }

  /**
   * Create new instance of notification based on given.
   * {@link StockNotification}.
   *
   * @param id               id uuid parameter.
   * @param quoteNumber      quoteNumber string parameter.
   * @param customerId       customerId string parameter.
   * @param customerName     customerName string parameter.
   * @param hfrCode          hfrCode string parameter.
   * @param elmisOrderNumber elmisOrderNumber string parameter.
   * @param notificationDate notificationDate string parameter.
   * @param zone             zone string parameter.
   * @param processingDate   processingDate string parameter.
   * @param comment          comment string parameter.
   * @param requisitionId    requisitionId uui parameter.
   *
   * @return instance of StockNotification.
   */
  public static StockNotification newStockNotification(UUID id, String quoteNumber,
                                                       String customerId,
                                                       String customerName,
                                                       String hfrCode,
                                                       String elmisOrderNumber,
                                                       String notificationDate,
                                                       String processingDate,
                                                       String zone, String comment,
                                                       UUID requisitionId) {
    return new StockNotification(id, quoteNumber,
        customerId, customerName,
        hfrCode, elmisOrderNumber,
        notificationDate, processingDate,
        zone, comment, requisitionId);
  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(StockNotificationDto exporter) {
    exporter.setId(id);
    exporter.setQuoteNumber(quoteNumber);
    exporter.setCustomerId(customerId);
    exporter.setCustomerName(customerName);
    exporter.setHfrCode(hfrCode);
    exporter.setElmisOrderNumber(elmisOrderNumber);
    exporter.setNotificationDate(notificationDate);
    exporter.setProcessingDate(processingDate);
    exporter.setZone(zone);
    exporter.setComment(comment);
    exporter.setRequisitionId(requisitionId);

    if (exporter.getLineItems() != null) {
      exporter.setLineItems(lineItems
          .stream()
          .map(StockNotificationLineItemDto::newInstance)
          .collect(Collectors.toList())
      );
    }
  }

  /**
   * Exports data from the given stock notification to the instance that implement
   * {@link Exporter} interface.
   */
  public void export(Exporter exporter) {
    exporter.setId(getId());
  }

  public interface Exporter {

    void setId(UUID id);

    void setQuoteNumber(String quoteNumber);

    void setCustomerId(String customerId);

    void setCustomerName(String customerName);

    void setHfrCode(String hfrCode);

    void setElmisOrderNumber(String elmisOrderNumber);

    void setNotificationDate(String notificationDate);

    void setProcessingDate(String processingDate);

    void setZone(String zone);

    void setComment(String comment);

    void setRequisitionId(UUID requisitionId);

    void setLineItems(List<StockNotificationLineItemDto> lineItems);
  }

  public interface Importer {

    UUID getId();

    String getQuoteNumber();

    String getCustomerId();

    String getHfrCode();

    String getElmisOrderNumber();

    String getNotificationDate();

    String getProcessingDate();

    String getZone();

    String getComment();

    UUID getRequisitionId();

    String getCustomerName();

    List<StockNotificationLineItemDto> getLineItems();

  }

  private static void validateLineItems(List<?> lineItems) {
    if (isEmpty(lineItems)) {
      throw new ValidationMessageException(MessageKeys.ERROR_LINE_ITEM_REQUIRED);
    }
  }

}
