/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_EMPTY;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.openlmis.common.dto.PatientDataDto;

@Entity
@Table(name = "data_responses", schema = "common")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonSerialize(include = NON_EMPTY)
public class DataResponse extends BaseEntity {

  @Setter
  @Getter
  private Integer page;

  @Setter
  @Getter
  private Integer pageCount;

  @Setter
  @Getter
  private Integer total;

  @Setter
  @Getter
  private Integer pageSize;

  @OneToMany(cascade = CascadeType.PERSIST,
      mappedBy = "dataResponse",
      orphanRemoval = true)
  @Setter
  @Getter
  private List<PatientData> patientData;

  /**
   * Constructor.
   *
   * @param page      name of the interface app
   * @param pageCount name of the interface app
   * @param total     name of the interface app
   * @param pageSize  active of the interface app
   */
  public DataResponse(Integer page,
                      Integer pageCount,
                      Integer total,
                      Integer pageSize) {
    this.page = page;
    this.pageCount = pageCount;
    this.total = total;
    this.pageSize = pageSize;
  }

  /**
   * Creates new instance based on data from the importer.
   */
  public static DataResponse newInstance(DataResponse.Importer importer) {

    DataResponse dataResponse = new DataResponse();

    dataResponse.setPage(importer.getPage());
    dataResponse.setPageCount(importer.getPageCount());
    dataResponse.setTotal(importer.getTotal());
    dataResponse.setPageSize(importer.getPageSize());

    return dataResponse;
  }

  /**
   * Copy values of attributes into new or updated ApplicationInterface.
   *
   * @param dataList list of interface data sets.
   */
  public void updateFrom(List<PatientData> dataList) {

    if (dataList != null) {
      patientData.clear();
      patientData.addAll(dataList);
    }

  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(DataResponse.Exporter exporter) {

    exporter.setId(getId());
    exporter.setPage(getPage());
    exporter.setTotal(getTotal());
    exporter.setPageCount(getPageCount());
    exporter.setPageSize(getPageSize());
    exporter.setPatientData(getPatientData());

  }

  public interface Exporter extends
      BaseTimestampedEntity.BaseTimestampedExporter {

    void setPage(Integer page);

    void setPageCount(Integer pageCount);

    void setTotal(Integer total);

    void setPageSize(Integer pageSize);

    void setPatientData(List<PatientData> patientData);

  }

  public interface Importer extends BaseTimestampedEntity.BaseTimestampedImporter {
    Integer getPage();

    Integer getPageCount();

    Integer getTotal();

    Integer getPageSize();

    List<PatientDataDto> getPatientData();

  }

}
