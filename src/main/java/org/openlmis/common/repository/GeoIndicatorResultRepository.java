/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.openlmis.common.dto.GeoFacilityIndicatorDto;
import org.springframework.stereotype.Repository;

@Repository
public class GeoIndicatorResultRepository {

  @PersistenceContext
  private EntityManager entityManager;

  private static final String SQL_QUERY = " WITH BaseData AS ("
      + "  SELECT distinct "
      + "        Cast(null as varchar) rnrId,  "
      + "        Cast(f.id as varchar) id,"
      + "        f.name,"
      + "        CAST(false AS Boolean) reported "
      + "    FROM"
      + "        referencedata.facilities f"
      + "            INNER JOIN referencedata.requisition_group_members rgm "
      + " ON rgm.facilityid = f.id"
      + "            INNER JOIN referencedata.supported_programs ps "
      + " ON ps.facilityId = f.id"
      + "            AND ps.programId = :programId"
      + "            INNER JOIN referencedata.requisition_group_program_schedules rgps "
      + " ON rgps.requisitionGroupId = rgm.requisitionGroupId"
      + "            INNER JOIN referencedata.processing_periods period "
      + " ON period.processingscheduleid = rgps.processingscheduleid"
      + "            LEFT JOIN requisition.requisitions r "
      + " ON r.facilityId = f.id AND r.programId = ps.programId"
      + "            AND r.processingperiodId = period.id "
      + " AND r.emergency = false AND r.status NOT IN ('INITIATED', 'SUBMITTED', 'SKIPPED')"
      + "    WHERE"
      + "            f.active = true AND ps.active = true"
      + "      AND period.id = :processingPeriodId "
      + "      AND r.id IS NULL"
      + "      AND f.geographicZoneId = :zoneId"
      + " ), ReportingData AS ("
      + "    SELECT"
      + "        DISTINCT Cast(rq.id as varchar) rnrId, "
      + "        Cast(f.id as varchar) id,"
      + "        f.name,"
      + "        CAST(true AS Boolean) reported "
      + "    FROM "
      + "        referencedata.facilities f"
      + "            JOIN ("
      + "            SELECT facilityId, r.id"
      + "            FROM requisition.requisitions r"
      + "            WHERE r.programId = :programId "
      + "              AND r.processingperiodid = :processingPeriodId "
      + "              AND emergency = false"
      + "              AND status NOT IN ('INITIATED', 'SUBMITTED', 'SKIPPED')"
      + "        ) rq ON rq.facilityId = f.id"
      + "            JOIN referencedata.supported_programs ps ON ps.facilityId = f.id"
      + "            JOIN referencedata.geographic_zones gz ON gz.id = f.geographicZoneId"
      + "            JOIN referencedata.requisition_group_members rgm ON rgm.facilityId = f.id"
      + "            JOIN referencedata.requisition_groups rg ON rg.id = rgm.requisitiongroupid"
      + "            JOIN referencedata.supervisory_nodes sn ON sn.id = rg.supervisorynodeid"
      + "            JOIN referencedata.role_assignments ra "
      + " ON ra.supervisorynodeid = sn.id OR ra.supervisorynodeid = sn.parentid"
      + "    WHERE"
      + "            f.enabled = true "
      + "      AND f.geographicZoneId = :zoneId "
      + "      AND ra.programid = :programId "
      + " ) "
      + " SELECT * FROM BaseData"
      + " UNION ALL "
      + " SELECT * FROM ReportingData"
      + " ORDER BY name;";

  /**
   * Fetches facility indicators for a given program,
   * processing period, and geographic zone.
   *
   * @param programId Unique identifier of the program.
   * @param period    Processing period identifier.
   * @param zoneId    Geographic zone identifier.
   * @return List of GeoFacilityIndicatorDto objects.
   */
  public List<GeoFacilityIndicatorDto> getFacilityIndicator(String programId,
                                                            String period,
                                                            String zoneId) {
    try (Session session = entityManager.unwrap(Session.class)) {
      Query query = session.createNativeQuery(SQL_QUERY);
      setQueryParameters(query, programId, period, zoneId);
      List<Object[]> resultList = query.getResultList();
      return mapResultListToDtoList(resultList);
    }
  }

  private void setQueryParameters(Query query,
                                  String programId,
                                  String processingPeriodId,
                                  String zoneId) {
    query.setParameter("programId", programId);
    query.setParameter("processingPeriodId", processingPeriodId);
    query.setParameter("zoneId", zoneId);
  }

  private List<GeoFacilityIndicatorDto>
      mapResultListToDtoList(List<Object[]> resultList) {
    List<GeoFacilityIndicatorDto> geoZoneReportingRates = new ArrayList<>();

    for (Object[] row : resultList) {
      GeoFacilityIndicatorDto
          geoZoneReportingRate = new GeoFacilityIndicatorDto();
      // Map values from row to geoZoneReportingRate object
      geoZoneReportingRate.setRnrId((String) row[0]);
      geoZoneReportingRate.setId((String) row[1]);
      geoZoneReportingRate.setName((String) row[2]);
      geoZoneReportingRate.setReported((Boolean) row[3]);
      geoZoneReportingRates.add(geoZoneReportingRate);
    }
    return geoZoneReportingRates;
  }
}
