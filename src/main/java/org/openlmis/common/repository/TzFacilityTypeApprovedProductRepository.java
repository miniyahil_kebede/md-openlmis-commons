/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.openlmis.common.dto.FacilityTypeApprovedProductDto;
import org.springframework.stereotype.Repository;

@Repository
public class TzFacilityTypeApprovedProductRepository {

  @PersistenceContext
  private EntityManager entityManager;

  /**
   * Save FacilityTypeApprovedProduct object.
   *
   * @param productDto productDto to update FacilityTypeApprovedProduct.
   */
  @Transactional
  public void updatePriorityDrug(FacilityTypeApprovedProductDto
                                     productDto) {

    entityManager.createNativeQuery(
            "UPDATE referencedata.facility_type_approved_products "
                + "SET priorityDrug = ? "
                + "WHERE id = ?")
        .setParameter(1, productDto.getPriorityDrug())
        .setParameter(2, productDto.getId())
        .executeUpdate();
  }

  private static final String SQL_QUERY = "SELECT "
      + "    CAST(gzz.id AS VARCHAR) AS id, "
      + "    CAST(gzz.priorityDrug AS BOOLEAN) AS priorityDrug "
      + " FROM referencedata.facility_type_approved_products gzz "
      + " WHERE gzz.id = :id ";

  /**
   * Retrieves a list of FacilityTypeApprovedProductDto objects by their UUID.
   *
   * @param id The UUID of the approved product(s) to retrieve.
   * @return A list of objects matching the given UUID.
   */
  @Transactional
  public List<FacilityTypeApprovedProductDto> findById(UUID id) {
    try (Session session = entityManager.unwrap(Session.class)) {
      Query query = session.createNativeQuery(SQL_QUERY);
      setQueryParameters(query, id);
      List<Object[]> resultList = query.getResultList();
      return mapResultListToDtoList(resultList);
    }
  }

  /**
   * Sets the 'id' parameter for the given query.
   *
   * @param query The query to set the parameter for.
   * @param id    The UUID parameter value to set.
   */
  private void setQueryParameters(Query query, UUID id) {
    query.setParameter("id", id);
  }

  /**
   * Maps a list of Object arrays to a list of objects.
   *
   * @param resultList The list of Object arrays to map.
   * @return A list of FacilityTypeApprovedProductDto objects.
   */
  private List<FacilityTypeApprovedProductDto>
      mapResultListToDtoList(List<Object[]> resultList) {
    List<FacilityTypeApprovedProductDto>
        approvedProductDtos = new ArrayList<>();

    for (Object[] row : resultList) {
      FacilityTypeApprovedProductDto
          approvedProductDto = new FacilityTypeApprovedProductDto();
      approvedProductDto.setId((String) row[0]);
      approvedProductDto.setPriorityDrug((Boolean) row[1]);
      approvedProductDtos.add(approvedProductDto);
    }

    return approvedProductDtos;
  }

  private static final String SQL_QUERY2 = "SELECT "
      + "    CAST(gzz.id AS VARCHAR) AS id, "
      + "    CAST(gzz.priorityDrug AS BOOLEAN) AS priorityDrug "
      + " FROM referencedata.facility_type_approved_products gzz ";

  /**
   * Retrieves all facility type approved product data.
   *
   * @return A list of FacilityTypeApprovedProductDto objects containing all the data.
   */
  @Transactional
  public List<FacilityTypeApprovedProductDto> findAllData() {
    try (Session session = entityManager.unwrap(Session.class)) {
      Query query = session.createNativeQuery(SQL_QUERY2);
      List<Object[]> resultList = query.getResultList();
      return mapResultListToDtoList(resultList);
    }
  }

}
