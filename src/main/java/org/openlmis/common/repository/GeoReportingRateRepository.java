/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.openlmis.common.dto.GeoZoneReportingRateDto;
import org.springframework.stereotype.Repository;

@Repository
public class GeoReportingRateRepository {

  @PersistenceContext
  private EntityManager entityManager;

  private static final
      String FROM_REFERENCEDATA_FACILITIES = "    "
      + " FROM referencedata.facilities  ";

  private static final String SQL_QUERY = "SELECT "
      + "   Cast(gzz.id as varchar) id , "
      + "    gzz.name, "
      + "    gjson.geometry, "
      + "    COALESCE(expected.count, 0) as expected, "
      + "    COALESCE(total.count, 0) as total, "
      + "    COALESCE(ever.count, 0) as ever, "
      + "    COALESCE(period.count, 0) as period "
      + "FROM referencedata.geographic_zones gzz "
      + "LEFT JOIN common.geographic_zone_geojson gjson"
      + " ON gzz.id = gjson.zoneId "
      + "LEFT JOIN ( "
      + "   SELECT geographicZoneId, COUNT(*) as count "
      + FROM_REFERENCEDATA_FACILITIES
      + "    JOIN referencedata.supported_programs ps ON ps.facilityId = facilities.id  "
      + "    JOIN referencedata.geographic_zones gz ON gz.id = facilities.geographicZoneId  "
      + "    JOIN referencedata.requisition_group_members rgm ON rgm.facilityId = facilities.id  "
      + "    JOIN referencedata.requisition_group_program_schedules rgps "
      + " ON rgps.requisitionGroupId = rgm.requisitionGroupId AND rgps.programId = ps.programId  "
      + "    JOIN referencedata.processing_periods pp "
      + " ON pp.processingscheduleId = rgps.processingscheduleId AND pp.id = :processingPeriodId  "
      + "   WHERE gz.levelId = (SELECT id "
      + " FROM referencedata.geographic_levels ORDER BY levelnumber DESC LIMIT 1) "
      + "  AND ps.programId = :programId  "
      + " GROUP BY geographicZoneId  "
      + " ) expected ON gzz.id = expected.geographicZoneId  "
      + " LEFT JOIN (  "
      + "    SELECT geographicZoneId, COUNT(*) as count  "
      + FROM_REFERENCEDATA_FACILITIES
      + "    JOIN referencedata.geographic_zones gz"
      + " ON gz.id = facilities.geographicZoneId  "
      + "    WHERE gz.levelId = (SELECT id FROM referencedata.geographic_levels "
      + " ORDER BY levelnumber DESC LIMIT 1) "
      + "    GROUP BY geographicZoneId  "
      + " ) total ON gzz.id = total.geographicZoneId  "
      + " LEFT JOIN (  "
      + "    SELECT geographicZoneId, COUNT(*) as count  "
      + FROM_REFERENCEDATA_FACILITIES
      + "    JOIN referencedata.supported_programs ps ON ps.facilityId = facilities.id  "
      + "    JOIN referencedata.geographic_zones gz ON gz.id = facilities.geographicZoneId  "
      + "    WHERE ps.programId = :programId AND facilities.id IN (  "
      + "        SELECT facilityId FROM requisition.requisitions WHERE programId = :programId "
      + "    )  "
      + "    GROUP BY geographicZoneId  "
      + " ) ever ON gzz.id = ever.geographicZoneId  "
      + " LEFT JOIN (  "
      + "    SELECT geographicZoneId, COUNT(*) as count  "
      + FROM_REFERENCEDATA_FACILITIES
      + "    JOIN referencedata.supported_programs ps ON ps.facilityId = facilities.id  "
      + "    JOIN referencedata.geographic_zones gz ON gz.id = facilities.geographicZoneId  "
      + "    WHERE ps.programId = :programId AND facilities.id IN (  "
      + "        SELECT facilityId FROM requisition.requisitions "
      + " WHERE processingPeriodId = :processingPeriodId AND programId = :programId  "
      + "        AND status NOT IN ('INITIATED', 'SUBMITTED', 'SKIPPED')  "
      + "        AND emergency = FALSE  "
      + "    )  "
      + "    GROUP BY geographicZoneId  "
      + ") period ON gzz.id = period.geographicZoneId  "
      + "ORDER BY gzz.name;";

  /**
   * Retrieves geographic zone rr for a specific program and processing period.
   *
   * @param programId          The ID of the program.
   * @param period The ID of the processing period.
   * @return List of GeoZoneReportingRateDto objects.
   */
  public List<GeoZoneReportingRateDto> getGeoReportingRate(String programId,
                                                           String period) {
    try (Session session = entityManager.unwrap(Session.class)) {
      Query query = session.createNativeQuery(SQL_QUERY);
      setQueryParameters(query, programId, period);
      List<Object[]> resultList = query.getResultList();
      return mapResultListToDtoList(resultList);
    }
  }

  /**
   * Sets the parameters for the native SQL query.
   *
   * @param query              The Hibernate Query object.
   * @param programId          The ID of the program.
   * @param processingPeriodId The ID of the processing period.
   */
  private void setQueryParameters(Query query, String programId,
                                  String processingPeriodId) {
    query.setParameter("programId", programId);
    query.setParameter("processingPeriodId", processingPeriodId);
  }

  /**
   * Maps the result list from the native SQL query
   * to a list of GeoZoneReportingRateDto objects.
   *
   * @param resultList The result list from the SQL query.
   * @return List of GeoZoneReportingRateDto objects.
   */
  private List<GeoZoneReportingRateDto>
      mapResultListToDtoList(List<Object[]> resultList) {
    List<GeoZoneReportingRateDto> geoZoneReportingRates = new ArrayList<>();

    for (Object[] row : resultList) {
      GeoZoneReportingRateDto
          geoZoneReportingRate = new GeoZoneReportingRateDto();
      // Map values from row to geoZoneReportingRate object
      geoZoneReportingRate.setId((String) row[0]);
      geoZoneReportingRate.setName((String) row[1]);
      geoZoneReportingRate.setGeometry((String) row[2]);
      geoZoneReportingRate.setExpected((BigInteger) row[3]);
      geoZoneReportingRate.setTotal((BigInteger) row[4]);
      geoZoneReportingRate.setEver((BigInteger) row[5]);
      geoZoneReportingRate.setPeriod((BigInteger) row[6]);
      geoZoneReportingRates.add(geoZoneReportingRate);
    }

    return geoZoneReportingRates;
  }

}
