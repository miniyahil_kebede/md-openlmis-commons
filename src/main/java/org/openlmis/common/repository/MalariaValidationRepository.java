/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.util.UUID;
import javax.transaction.Transactional;
import org.openlmis.common.domain.MalariaValidation;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MalariaValidationRepository
    extends PagingAndSortingRepository<MalariaValidation, UUID> {

  @Query(value = "SELECT * FROM common.malaria_validations s "
      + " where s.requisitionId=:requisitionId", nativeQuery = true)
  Iterable<MalariaValidation> findByRequisitionId(
      @Param("requisitionId") UUID requisitionId);

  @Transactional
  @Modifying
  @Query(value = "DELETE FROM common.malaria_validations s "
      + " WHERE s.requisitionId=:requisitionId", nativeQuery = true)
  void deleteByRequisitionId(@Param("requisitionId") UUID requisitionId);

}
