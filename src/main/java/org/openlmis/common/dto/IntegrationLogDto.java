/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.domain.IntegrationLog;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class IntegrationLogDto extends BaseDto
    implements IntegrationLog.Exporter, IntegrationLog.Importer {

  private String sourceTransactionId;
  private UUID destinationId;
  private String integrationName;
  private String errorMessage;
  private Integer errorCode;
  private Boolean resolved;

  /**
   * Creates new set of IntegrationLogDto based on
   * {@link IntegrationLog} iterable.
   */
  public static Set<IntegrationLogDto> newInstance(
      Iterable<IntegrationLog> iterable) {
    Set<IntegrationLogDto> logDtos = new HashSet<>();
    iterable.forEach(i -> logDtos.add(newInstance(i)));
    return logDtos;
  }

  /**
   * Creates new instance of IntegrationLogDto based on {@link IntegrationLog}.
   */
  public static IntegrationLogDto newInstance(IntegrationLog log) {
    IntegrationLogDto logDto = new IntegrationLogDto();
    log.export(logDto);
    return logDto;
  }

}
