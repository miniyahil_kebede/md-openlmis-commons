/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import java.util.UUID;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.dto.requisition.BasicOrderableDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Embeddable
public class VersionEntityReference extends BasicOrderableDto {

  private static final String UUID_TYPE = "pg-uuid";

  @Type(type = UUID_TYPE)
  private UUID id;

  private Long versionNumber;

  /**
   * Copy constructor.
   *
   * @param original an original version entity reference with data that will be placed in
   *                 a new version entity reference.
   */
  public VersionEntityReference(OrderableDto original) {
    this.id = original.getId();
    this.versionNumber = original.getVersionNumber();
  }
}
