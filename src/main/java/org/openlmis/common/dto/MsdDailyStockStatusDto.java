/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.domain.MsdDailyStockStatus;
import org.openlmis.common.domain.MsdDailyStockStatusValue;


@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MsdDailyStockStatusDto extends BaseDto
    implements MsdDailyStockStatus.Importer, MsdDailyStockStatus.Exporter {

  @Getter
  @Setter
  @NotBlank
  private String ilNumber;

  @Getter
  @NotEmpty
  private Set<MsdDailyStockStatusValueDto> values;

  /**
   * Creates a new MsdDailyStockStatus DTO from the entity.
   *
   * @param stockStatus the stock status to export into DTO
   *
   * @return created MsdDailyStockStatus DTO
   */
  public static MsdDailyStockStatusDto newInstance(MsdDailyStockStatus stockStatus) {
    MsdDailyStockStatusDto stockStatusDto = new MsdDailyStockStatusDto();
    stockStatus.export(stockStatusDto);
    return stockStatusDto;
  }

  @Override
  public void setValues(Set<MsdDailyStockStatusValue> stockStatusValues) {
    this.values = stockStatusValues.stream()
        .map(value -> MsdDailyStockStatusValueDto.newInstance(value))
        .collect(Collectors.toSet());
  }
}
