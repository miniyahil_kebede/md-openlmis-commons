/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.requisition;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.openlmis.common.dto.RequisitionTemplateDto;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.dto.referencedata.ProcessingPeriodDto;
import org.openlmis.common.dto.referencedata.ProgramDto;
import org.openlmis.common.dto.requisition.RequisitionStatus;
/**
 * RequisitionDto.
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequisitionDto extends RequisitionPeriodDto {

  private ZonedDateTime createdDate;

  private ZonedDateTime modifiedDate;

  private RequisitionStatus status;

  private Boolean emergency;

  private UUID supervisoryNode;

  private Map<String, Object> extraData;
  private FacilityDto facility;

  private ProgramDto program;
  private ProcessingPeriodDto processingPeriod;
  private List<RequisitionLineItemDto> requisitionLineItems;

  private Set<OrderableDto> availableFullSupplyProducts;

  private Set<OrderableDto> availableNonFullSupplyProducts;

  private BigDecimal allocatedBudget;
  @Getter
  @Setter
  private RequisitionTemplateDto template;


  private String sourceApplication;

  private UUID supplyingFacility;

  private Map<String, StatusLogEntry> statusChanges;

  private List<StatusChange> statusHistory;

  private LocalDate datePhysicalStockCountCompleted;

  private List<ReasonDto> stockAdjustmentReasons;

  private String approvalStatus;

  private UUID supplyingFacilityId;

  /**
   * Release the requisition.
   */
  public void release(UUID releaser) {
    status = RequisitionStatus.RELEASED;
    approvalStatus = "RELEASED";
    setModifiedDate(ZonedDateTime.now());
  }

  /**
   * Filter out requisitionLineItems that are skipped and not-full supply.
   *
   * @return non-skipped full supply requisition line items
   */
  public List<RequisitionLineItemDto> getNonSkippedFullSupplyRequisitionLineItems() {
    return this.requisitionLineItems.stream()
            .filter(line -> !line.getSkipped())
            .filter(line -> !line.isNonFullSupply(program))
            .collect(Collectors.toList());
  }

  /**
   * Filter out requisitionLineItems that are skipped and full supply.
   *
   * @return non-skipped non-full supply requisition line items
   */
  public List<RequisitionLineItemDto> getNonSkippedNonFullSupplyRequisitionLineItems() {
    return this.requisitionLineItems.stream()
            .filter(line -> !line.getSkipped())
            .filter(line -> line.isNonFullSupply(program))
            .collect(Collectors.toList());
  }

  /**
   * Calculates combined cost of all requisition line items.
   *
   * @return sum of total costs.
   */
  public BigDecimal getTotalCost() {
    return calculateTotalCostForLines(requisitionLineItems);
  }

  /**
   * Calculates combined cost of non-full supply non-skipped requisition line items.
   *
   * @return sum of total costs.
   */
  public BigDecimal getNonFullSupplyTotalCost() {
    return calculateTotalCostForLines(getNonSkippedNonFullSupplyRequisitionLineItems());
  }

  /**
   * Calculates combined cost of full supply non-skipped requisition line items.
   *
   * @return sum of total costs.
   */
  public BigDecimal getFullSupplyTotalCost() {
    return calculateTotalCostForLines(getNonSkippedFullSupplyRequisitionLineItems());
  }

  /**
   * Returns information if it is CHAM facility or not.
   *
   * @return Boolean value.
   */
  public Boolean isChamFacility() {
    return !program.getCode().equals("tb") && facility.getOperator().getCode().equals("CHAM")
            && !status.isPreAuthorize();
  }

  private BigDecimal calculateTotalCostForLines(List<RequisitionLineItemDto> requisitionLineItems) {
    if (requisitionLineItems.isEmpty()) {
      return BigDecimal.ZERO;
    }

    BigDecimal money = requisitionLineItems.stream()
            .map(RequisitionLineItemDto::getTotalCost)
            .filter(Objects::nonNull)
            .reduce(Money.zero(CurrencyUnit.USD), Money::plus).getAmount();

    return money;
  }
  /**
   * Retrieves the first approved status change from the status history of the requisition DTO.
   *
   * @return The first approved status change if found, otherwise null.
   */

  public StatusChange getApprovedStatusChange() {
    List<StatusChange> approvedStatusChanges = this.getStatusHistory().stream()
            .filter(statusChange -> statusChange.getStatus() == RequisitionStatus.APPROVED)
            .collect(Collectors.toList());

    return approvedStatusChanges.isEmpty() ? null : approvedStatusChanges.get(0);
  }
  /**
   * Retrieves the first approved status change from the status history of the requisition DTO.
   *
   * @return The first approved status change if found, otherwise null.
   */

  public StatusChange getAuthorizedStatusChange() {
    List<StatusChange> authorizedStatusChanges = this.getStatusHistory().stream()
            .filter(statusChange -> statusChange.getStatus() == RequisitionStatus.AUTHORIZED)
            .collect(Collectors.toList());

    return authorizedStatusChanges.isEmpty() ? null : authorizedStatusChanges.get(0);
  }
}
