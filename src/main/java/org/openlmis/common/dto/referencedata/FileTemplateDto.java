/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.referencedata;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.domain.BaseEntity;
import org.openlmis.common.util.FileColumnKeyPath;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public final class FileTemplateDto extends BaseEntity {

  @Getter
  @Setter
  private String filePrefix;

  @Getter
  @Setter
  private Boolean headerInFile;

  @Getter
  @Setter
  private TemplateType templateType;

  @Getter
  @Setter
  private List<FileColumnDto> fileColumns;

  /**
   * Updates itself using data from {@link FileTemplateDto.Importer}.
   *
   * @param importer instance of {@link FileTemplateDto.Importer}
   */
  public void importDto(Importer importer) {
    id = importer.getId();
    filePrefix = importer.getFilePrefix();
    headerInFile = importer.getHeaderInFile();
    templateType = importer.getTemplateType();

    fileColumns.clear();
    if (importer.getFileColumns() != null) {
      for (FileColumnDto.Importer columnImporter : importer.getFileColumns()) {
        FileColumnDto column = FileColumnDto.newInstance(columnImporter);
        column.setFileTemplate(this);

        fileColumns.add(column);
      }
    }
  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(Exporter exporter) {
    exporter.setId(id);
    exporter.setFilePrefix(filePrefix);
    exporter.setHeaderInFile(headerInFile);
    exporter.setTemplateType(templateType);
  }

  /**
   * Returns columns matching key path.
   * @param keyPaths keyPaths
   * @return Optional FileColumn
   */
  public Optional<FileColumnDto> findColumn(List<FileColumnKeyPath> keyPaths) {
    return this.fileColumns.stream()
        .filter(column -> keyPaths.contains(FileColumnKeyPath.fromString(column.getKeyPath())))
        .findFirst();
  }

  public interface Exporter {
    void setId(UUID id);

    void setFilePrefix(String filePrefix);

    void setHeaderInFile(Boolean headerInFile);

    void setTemplateType(TemplateType templateType);

  }

  public interface Importer {

    UUID getId();

    String getFilePrefix();

    Boolean getHeaderInFile();

    List<FileColumnDto.Importer> getFileColumns();

    TemplateType getTemplateType();

  }
}