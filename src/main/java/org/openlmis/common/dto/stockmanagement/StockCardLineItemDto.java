/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.stockmanagement;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.requisition.ReasonType;
import org.openlmis.common.service.stockmanagement.StockCardLineItemReasonDto;

/**
 * StockCardLineItemDto class.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class StockCardLineItemDto {
  private FacilityDto source;
  private FacilityDto destination;



  private Integer quantity;


  private StockCardLineItemReasonDto reason;

  private String sourceFreeText;
  private String destinationFreeText;
  private String documentNumber;
  private String reasonFreeText;
  private String signature;


  @JsonFormat(shape = STRING, pattern = "yyyy-MM-dd")
  private LocalDate occurredDate;

  private ZonedDateTime processedDate;

  private UUID userId;

  private Integer stockOnHand;









  /**
   * Returns quantity value with correct sign depending on reason type.
   *
   * @return quantity value, is negative for Debit reason
   */
  public Integer getQuantityWithSign() {
    if (null == this.getQuantity()) {
      return 0;
    }
    return this.isPositive()
      ? this.getQuantity()
      : this.getQuantity() * -1;
  }

  /**
   * Checks if assigned reason has tag assigned.
   *
   * @param tag string with tag value
   * @return true if there is reason assigned and has given tag
   */
  public boolean containsTag(String tag) {
    return null != this.getReason() && this.getReason().getTags().contains(tag);
  }

  /**
   * Checks if line item is physical inventory.
   *
   * @return true if is physical inventory
   */
  public boolean isPhysicalInventory() {
    return source == null && destination == null && reason == null;
  }

  /**
   * Checks if line item will add quantity to SoH.
   *
   * @return true if is physical inventory
   */
  public boolean isPositive() {
    boolean hasSource = source != null;
    boolean isCredit = reason != null && reason.getReasonType() == ReasonType.CREDIT.name();
    return hasSource || isCredit;
  }
}
