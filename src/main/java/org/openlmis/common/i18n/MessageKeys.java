/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.i18n;

import java.util.Arrays;

public abstract class MessageKeys {

  public static final String MSD_DAILY_STOCK_STATUS = "msdDailyStockStatus";

  public static final String TZ_REQUISITION_TEMPLATE = "tzRequisitionTemplate";

  public static final String ERROR_CONVERTING_MULTIPLE_REQUISITIONS
      = "convert.multiple.requisition";

  public static final String ERROR_REQUISITION_NOT_FOUND
      = "common.error.requisition.not.found";

  public static final String ERROR_ORDER_NOT_FOUND
      = "common.error.order.not.found";

  public static final String MUST_CONTAIN_VALUE
      = "common.error.requisition.must.contain.value";

  public static final String ERROR_IO
      = "input.output.error";

  protected static final String PROCESSING_PERIOD = "processingPeriod";

  private static final String DELIMITER = ".";

  public static final String SERVICE_PREFIX = "common";

  protected static final String VALIDATION = "validation";

  static final String SERVICE_ERROR_PREFIX = join(SERVICE_PREFIX, "error");

  protected static final String NULL = "null";

  private static final String ERROR = "error";

  protected static final String AND = "and";

  private static final String WIDGET = "widget";

  private static final String JAVERS = "javers";

  public static final String ID = "id";

  private static final String USERS = "users";

  public static final String CODE = "code";

  public static final String MISMATCH = "mismatch";

  public static final String DUPLICATED = "duplicated";

  protected static final String INVALID = "invalid";

  public static final String NOT_FOUND = "notFound";

  protected static final String PARAMS = "params";

  public static final String ERROR_LINE_ITEM_REQUIRED = "lineItemIsRequired";

  public static final String ERROR_PREFIX = join(SERVICE_PREFIX, ERROR);

  public static final String ERROR_SERVICE_REQUIRED = ERROR_PREFIX + ".service.required";

  public static final String ERROR_SERVICE_OCCURED = ERROR_PREFIX + ".service.errorOccured";

  public static final String ERROR_WIDGET_NOT_FOUND = join(ERROR_PREFIX, WIDGET, NOT_FOUND);
  public static final String ERROR_WIDGET_ID_MISMATCH = join(ERROR_PREFIX, WIDGET, ID, MISMATCH);
  public static final String ERROR_WIDGET_CODE_DUPLICATED =
      join(ERROR_PREFIX, WIDGET, CODE, DUPLICATED);

  public static final String ERROR_INVALID_DATE_FORMAT =
      ERROR_PREFIX + ".validation.invalidDateFormat";
  public static final String ERROR_INVALID_BOOLEAN_FORMAT =
      ERROR_PREFIX + ".validation.invalidBooleanFormat";
  public static final String ERROR_INVALID_UUID_FORMAT =
      ERROR_PREFIX + ".validation.invalidUuidFormat";

  public static final String ERROR_INVALID_REQUISITION_STATUS =
      ERROR_PREFIX + ".validation.params.requisitionStatus.notValidStatus";
  public static final String ERROR_SEARCH_INVALID_PARAMS =
      ERROR_PREFIX + ".search.invalidParams";

  public static final String ERROR_JAVERS_EXISTING_ENTRY =
      join(ERROR_PREFIX, JAVERS, "entryAlreadyExists");
  protected static final String SERVICE_ERROR = join(SERVICE_PREFIX, "error");

  public static final String ERROR_MISSING_MANDATORY_FIELD = SERVICE_PREFIX
      + ".report.missingMandatoryField";
  private static final String PHYSICAL_INVENTORY_ERROR_PREFIX = SERVICE_ERROR
          + ".physicalInventory";
  protected static final String REQUISITION_ERROR = "requisition.error";


  public static final String ERROR_PHYSICAL_INVENTORY_FORMAT_NOT_ALLOWED =
          PHYSICAL_INVENTORY_ERROR_PREFIX + ".format.notAllowed";
  public static final String ERROR_JASPER_FILE_FORMAT = REQUISITION_ERROR + ".jasper.file.format";
  public static final String STATUS_CHANGE_USER_SYSTEM =
          REQUISITION_ERROR + ".statusChange.user.system";
  public static final String ERROR_JASPER_TEMPLATE_NOT_FOUND = ERROR_PREFIX
          + ".jasper.templateNotFound";
  public static final String ERROR_JASPER_REPORT_CREATION_WITH_MESSAGE =
      join(ERROR, "reportCreationWithMessage");
  public static final String ERROR_JASPER_REPORT_GENERATION = join(ERROR, "report", "generation");
  public static final String ERROR_JASPER_REPORT_FORMAT_UNKNOWN =
          join(ERROR, "report", "format", "unknown");
  public static final String USER_NOT_FOUND = USERS + ".notFound";

  public static final String USER_NOT_FOUND_BY_EMAIL = USERS + ".notFoundByEmail";

  public static final String ERROR_SEND_NOTIFICATION_FAILURE =
          ERROR_PREFIX + ".sendNotification.failure";

  public static final String PASSWORD_RESET_EMAIL_SUBJECT = "auth"
          + ".email.resetPassword.subject";
  public static final String PASSWORD_RESET_EMAIL_BODY = "auth"
          + ".email.resetPassword.body";
  public static final String ERROR_PERMISSION_CHECK_FAILED = ERROR_PREFIX
          + ".authorization.failed";
  public static final String ERROR_REPORT_ID_NOT_FOUND = ERROR_PREFIX + ".report.id.notFound";

  public static String join(String... params) {
    return String.join(DELIMITER, Arrays.asList(params));
  }

  protected MessageKeys() {
    throw new UnsupportedOperationException();
  }

}
