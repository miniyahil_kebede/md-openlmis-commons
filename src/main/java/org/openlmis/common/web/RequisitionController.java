/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.io.IOException;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.exception.ContentNotFoundMessageException;
import org.openlmis.common.exception.JasperReportViewException;
import org.openlmis.common.i18n.MessageKeys;
import org.openlmis.common.service.JasperReportsViewService;
import org.openlmis.common.service.PermissionService;
import org.openlmis.common.service.requisition.RequisitionService;
import org.openlmis.common.util.Message;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;


@Controller
@Transactional
@RequestMapping("/api/reports")
public class RequisitionController extends BaseController {

  private static final XLogger XLOGGER = XLoggerFactory.getXLogger(ReportsController.class);

  public static final String FORMAT = "format";

  @Autowired
  private PermissionService permissionService;


  @Autowired
  private JasperReportsViewService jasperReportsViewService;


  @Autowired
  private RequisitionService requisitionService;

  /**
   * Print out requisition as a PDF file.
   *
   * @param id The UUID of the requisition to print
   *     ResponseEntity containing the error description status.
   */
  @RequestMapping(value = "/requisitions/{id}/print", method = RequestMethod.GET)
  @ResponseStatus(HttpStatus.OK)
  public void print(@PathVariable("id") UUID id, @RequestParam(value = "lang",
      defaultValue = "fr") String lang,
                    HttpServletResponse response)
          throws JasperReportViewException, IOException, JRException {
    response.setContentType(MediaType.APPLICATION_PDF_VALUE);
    response.setHeader("Content-disposition",
            "inline; filename=requisition-" + id + ".pdf");
    RequisitionDto requisition = requisitionService.findOne(id);

    if (requisition == null) {
      throw new ContentNotFoundMessageException(
              new Message(MessageKeys.ERROR_REQUISITION_NOT_FOUND, id));
    }
    XLOGGER.info("lang: " + lang);

    permissionService.canViewRequisitions(requisition.getProgram().getId(),
            requisition.getFacility().getId());

    jasperReportsViewService.getRequisitionJasperReportView(requisition,
            response.getOutputStream());
  }

}
