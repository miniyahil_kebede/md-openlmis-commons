/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.util.Optional;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.domain.MsdDailyStockStatus;
import org.openlmis.common.dto.MsdDailyStockStatusDto;
import org.openlmis.common.dto.MsdDailyStockStatusValueDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.MsdDailyStockStatusMessageKeys;
import org.openlmis.common.service.MsdDailyStockStatusService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(MsdDailyStockStatusController.RESOURCE_PATH)
@RequiredArgsConstructor
public class MsdDailyStockStatusController extends BaseController {

  public static final String RESOURCE_PATH = ResourceNames.BASE_PATH + "/msdDailyStockStatus";

  private final MsdDailyStockStatusService stockStatusService;


  /**
   * Create a new stock status.
   *
   * @param stockStatusDto details of the stock status to be created.
   * @param bindingResult  validation results of the stock status details.
   *
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public MsdDailyStockStatusDto create(@RequestBody @Valid MsdDailyStockStatusDto stockStatusDto,
                                       BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MsdDailyStockStatusMessageKeys.ERROR_INVALID_PARAMS);
    }

    MsdDailyStockStatus stockStatus = stockStatusService.create(stockStatusDto);
    return MsdDailyStockStatusDto.newInstance(stockStatus);
  }

  /**
   * Retrieves all stock statuses with name similar to name parameter.
   *
   * @param queryParams name query parameter.
   * @param pageable    object used to encapsulate the pagination related values:
   *                    page, size and sort.
   *
   * @return Page of stock statuses matching query parameter.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<MsdDailyStockStatusValueDto> searchStockStatusValues(
      @RequestParam(required = false, name = "plant") Optional<String> queryParams,
      Pageable pageable) {

    return queryParams
        .map(filter -> stockStatusService.searchStockStatusValue(filter, pageable))
        .orElseGet(() -> stockStatusService.findAllStockStatusValue(pageable))
        .map(stockStatusValue -> MsdDailyStockStatusValueDto.newInstance(stockStatusValue));
  }
}
