/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.i18n.MessageKeys.NOT_FOUND;

import java.io.InputStream;
import java.util.List;
import java.util.UUID;
import javax.transaction.Transactional;
import org.openlmis.common.dto.ApplicationInterfaceExtensionDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.repository.ApplicationInterfaceRepository;
import org.openlmis.common.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller used to expose Application interfaces via HTTP.
 */
@Controller
@RequestMapping(ApplicationInterfaceController.RESOURCE_PATH)
@Transactional
public class ApplicationInterfaceController extends BaseController {
  public static final String RESOURCE_PATH = API_PATH + "/applicationInterfaces";

  @Autowired
  private ApplicationInterfaceRepository repository;

  @Autowired
  private DataService dataService;


  /**
   * Create a new application interface. If the ID is specified, ID will be ignored.
   *
   * @param interfaceDto application interface bound to request body
   * @return created application interface
   */
  @PostMapping("/save")
  public ResponseEntity<String> saveData(@RequestBody
                                         ApplicationInterfaceExtensionDto
                                             interfaceDto) {
    dataService.saveData(interfaceDto);
    return ResponseEntity.ok("Data saved successfully");
  }

  /**
   * Import Interface dataset from upload file.
   *
   * @return created application interface
   * @MultipartFile csv file with specified interface.
   */
  @PostMapping("/upload")
  public ResponseEntity<String> uploadCsvFile(@RequestBody MultipartFile file) {

    if (file.isEmpty()) {
      return new ResponseEntity<>(
          "Please select a file to upload", HttpStatus.BAD_REQUEST);
    }

    if ((!file.getContentType().equals("text/csv")
        && !file.getContentType().equals("application/vnd.ms-excel"))) {
      return new ResponseEntity<>(
          "Only CSV files are allowed", HttpStatus.BAD_REQUEST);
    }

    dataService.importDatasetToDB(file);

    return new ResponseEntity<>("File uploaded successfully", HttpStatus.OK);
  }

  /**
   * Get sample CSV.
   *
   * @getSampleCsV csv file with specified interface.
   */
  @RequestMapping("/sample")
  public ResponseEntity<Resource> getSampleCsV() {

    InputStream reportStream = dataService.generateSampleImportCsv();

    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION,
            "attachment; filename=samplecsv.csv"
        ).contentType(MediaType.parseMediaType("application/csv"))
        .body(new InputStreamResource(reportStream));

  }

  /**
   * Delete data for a given ID.
   *
   * @param id the ID of the data to delete
   * @return a ResponseEntity with a success message
   */
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<String> deleteData(@PathVariable UUID id) {
    dataService.deleteData(id);
    return ResponseEntity.ok("Data deleted successfully");
  }

  /**
   * Get all data.
   *
   * @return a ResponseEntity with a list of all data
   */
  @GetMapping("/getAll")
  public ResponseEntity<List<ApplicationInterfaceExtensionDto>> getAllData() {
    List<ApplicationInterfaceExtensionDto> interfaces = dataService.getAllData();
    return ResponseEntity.ok(interfaces);
  }

  /**
   * Get data by ID.
   *
   * @param id the ID of the data to retrieve
   * @return a ResponseEntity with the data corresponding to the given ID
   */
  @GetMapping("/{id}")
  public ResponseEntity<ApplicationInterfaceExtensionDto> getDataById(
      @PathVariable UUID id) {
    ApplicationInterfaceExtensionDto interfaceDto = dataService.getDataById(id);
    return ResponseEntity.ok(interfaceDto);
  }


  /**
   * Update data for a given ID.
   *
   * @param id           the ID of the data to update
   * @param interfaceDto the updated data
   * @return a ResponseEntity with a success message
   */
  @PutMapping("/{id}")
  public ResponseEntity<String> updateData(@PathVariable UUID id,
                                           @RequestBody
                                           ApplicationInterfaceExtensionDto interfaceDto) {
    dataService.updateData(id, interfaceDto);
    return ResponseEntity.ok("Data updated successfully");
  }

  /**
   * Deletes the specified application interface.
   */
  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteApplicationInterface(@PathVariable("id") UUID id) {
    if (!repository.existsById(id)) {
      throw new NotFoundException(NOT_FOUND);
    }

    repository.deleteById(id);
  }


}
