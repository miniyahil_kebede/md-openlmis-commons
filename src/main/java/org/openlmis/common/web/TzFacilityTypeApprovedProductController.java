/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.i18n.MessageKeys.NOT_FOUND;
import static org.openlmis.common.web.TzFacilityTypeApprovedProductController.RESOURCE_PATH;

import java.util.List;
import java.util.UUID;
import org.openlmis.common.dto.FacilityTypeApprovedProductDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.service.TzFacilityTypeApprovedProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@Transactional
@RequestMapping(RESOURCE_PATH)
public class TzFacilityTypeApprovedProductController
    extends BaseController {

  @Autowired
  private TzFacilityTypeApprovedProductService service;
  private static final String STATUS_UPDATED = "updated";

  private static final Logger LOGGER = LoggerFactory
      .getLogger(TzFacilityTypeApprovedProductController.class);

  public static final String RESOURCE_PATH = API_PATH
      + "/tzFacilityTypeApprovedProducts";

  /**
   * Updates a facility type approved product with the provided DTO.
   *
   * @param approvedProductDto The DTO containing the updated information.
   * @param facilityTypeApprovedProductId The UUID of the product to update.
   * @return A ResponseEntity with a status of OK if the update was successful.
   */
  @PutMapping("/{id}")
  @ResponseBody
  public ResponseEntity<FacilityTypeApprovedProductDto>
      updateFacilityTypeApprovedProduct(
      @RequestBody FacilityTypeApprovedProductDto approvedProductDto,
      @PathVariable("id") UUID facilityTypeApprovedProductId) {
    Profiler profiler = new Profiler(
        "UPDATE_FACILITY_TYPE_APPROVED_PRODUCT");
    profiler.setLogger(LOGGER);
    service.updateFacilityTypeApproved(approvedProductDto,
        facilityTypeApprovedProductId);
    return new ResponseEntity(STATUS_UPDATED, HttpStatus.OK);
  }

  /**
   * Retrieves a facility type approved product by its UUID.
   *
   * @param id The UUID of the facility type approved product to retrieve.
   * @return A ResponseEntity with a FacilityTypeApprovedProductDto.
   */
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public ResponseEntity<FacilityTypeApprovedProductDto> getGetApprovedBy(
      @PathVariable("id") UUID id) {
    List<FacilityTypeApprovedProductDto> dtos = service.getGetApprovedBy(id);

    if (dtos.get(0) == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      return new ResponseEntity(dtos, HttpStatus.OK);
    }
  }

  /**
   * Get all configurations.
   *
   * @return DhisOrderableConfig.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Iterable<FacilityTypeApprovedProductDto> geAll() {

    Iterable<FacilityTypeApprovedProductDto> configs = service.findAllData();

    if (configs == null) {
      throw new NotFoundException(NOT_FOUND);
    } else {
      return configs;
    }
  }
}
