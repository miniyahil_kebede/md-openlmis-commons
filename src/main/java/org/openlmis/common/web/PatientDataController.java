/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.i18n.MessageKeys.NOT_FOUND;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import javax.transaction.Transactional;
import org.openlmis.common.domain.DataResponse;
import org.openlmis.common.domain.PatientData;
import org.openlmis.common.dto.MalariaValidationDto;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.service.PatientDataService;
import org.openlmis.common.service.referencedata.FacilityReferenceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller used to expose Patient Data via HTTP.
 */
@Controller
@RequestMapping(PatientDataController.RESOURCE_PATH)
@Transactional
public class PatientDataController extends BaseController {
  public static final String RESOURCE_PATH = API_PATH + "/patient-data";

  @Autowired
  private PatientDataService service;

  @Autowired
  private FacilityReferenceDataService
      facilityReferenceDataService;

  /**
   * Returns pdf of defined object in response.
   *
   * @param jsonData String of data to from external system
   */
  @PostMapping
  public ResponseEntity<String> processData(@RequestBody String jsonData) {

    try {
      ObjectMapper objectMapper = new ObjectMapper();
      DataResponse dataResponse = objectMapper.readValue(jsonData,
          DataResponse.class);

      if (!dataResponse.getPatientData().isEmpty()) {

        service.saveOrUpdatePatientData(dataResponse.getPatientData());

      } else {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body("The Object is empty ");
      }

      return ResponseEntity.ok("Data saved successfully.");

    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
          .body("Error processing data: " + e.getMessage());
    }
  }

  /**
   * Returns pdf of defined object in response.
   *
   * @param facilityId    String of data to from external system
   * @param requisitionId String of data to from external system
   */
  @GetMapping("/{requisitionId}/{facilityId}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  Iterable<MalariaValidationDto> findMalariaData(
      @PathVariable UUID facilityId,
      @PathVariable UUID requisitionId) {

    FacilityDto facilityDto =
        facilityReferenceDataService.findOne(facilityId);
    //Get By Facility Code

    service.processMalariaDataForRequisition(facilityDto, requisitionId);
    //Return malaria data by requisition ID
    return MalariaValidationDto.newInstance(service.findByRequisitionId(requisitionId));

  }

  /**
   * Get all configurations.
   *
   * @return DhisOrderableConfig.
   */
  @GetMapping("/get-all")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Iterable<PatientData> geAllPatientData() {

    Iterable<PatientData> configs = service.findAllPatientsData();

    if (configs == null) {
      throw new NotFoundException(NOT_FOUND);
    } else {
      return configs;
    }
  }

}
