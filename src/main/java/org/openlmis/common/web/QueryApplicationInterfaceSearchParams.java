/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.openlmis.common.repository.custom.ApplicationInterfaceSearchParams;
import org.springframework.util.MultiValueMap;

@EqualsAndHashCode
@ToString
final class QueryApplicationInterfaceSearchParams implements
    ApplicationInterfaceSearchParams {

  private SearchParams queryParams;

  /**
   * Wraps map of query params into an object.
   */
  QueryApplicationInterfaceSearchParams(MultiValueMap<String, String> queryMap) {
    queryParams = new SearchParams(queryMap);
  }

  /**
   * Checks if query params are . Returns false if any provided param is not on supported list.
   */
  public boolean isEmpty() {
    return queryParams.isEmpty();
  }

}
