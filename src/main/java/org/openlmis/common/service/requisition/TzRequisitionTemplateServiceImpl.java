/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.requisition;

import java.util.List;
import java.util.UUID;
import lombok.NoArgsConstructor;
import org.openlmis.common.domain.TzRequisitionTemplate;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.i18n.TzRequisitionTemplateMessageKeys;
import org.openlmis.common.repository.TzRequisitionTemplateRepository;
import org.openlmis.common.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@NoArgsConstructor
@Service
public class TzRequisitionTemplateServiceImpl
        implements TzRequisitionTemplateService {

  @Autowired
  private TzRequisitionTemplateRepository tzRequisitionTemplateRepository;

  /**
   * Find all tz requisition templates.
   * Redo this function to merge those results with RequisitionTemplate
   * and return a proper DTO
   */
  @Override
  public List<TzRequisitionTemplate> findAll() {
    return tzRequisitionTemplateRepository.findAll();
  }

  /**
   * Find a tz requisition templated based on given ID.
   * Redo this function to merge this result with RequisitionTemplate
   * and return a proper DTO
   */
  @Override
  public TzRequisitionTemplate findOne(UUID id) {
    Message errorMessage = new Message(TzRequisitionTemplateMessageKeys.ERROR_NOT_FOUND);
    return tzRequisitionTemplateRepository
            .findById(id)
            .orElseThrow(() -> new NotFoundException(errorMessage));
  }

  /**
   *  Here make a call to requisitionTemplate table and an if to ensure,
   * that a requisitionTemplate with id tzRequisitionTemplate.requisitionTemplateId exists
   */
  @Override
  public TzRequisitionTemplate save(TzRequisitionTemplate tzRequisitionTemplate) {
    return tzRequisitionTemplateRepository.save(tzRequisitionTemplate);
  }

  @Override
  public void deleteById(UUID id) {
    tzRequisitionTemplateRepository.deleteById(id);
  }

  /**
   *  Update an existing tz requisition template.
   */
  @Override
  public TzRequisitionTemplate update(UUID id, TzRequisitionTemplate updatedTzRequisitionTemplate) {
    TzRequisitionTemplate existingTzRequisitionTemplate = findOne(id);

    existingTzRequisitionTemplate
            .setPatientsTabEnabled(updatedTzRequisitionTemplate.isPatientsTabEnabled());
    existingTzRequisitionTemplate
            .setRequisitionTemplateId(updatedTzRequisitionTemplate.getRequisitionTemplateId());

    return save(existingTzRequisitionTemplate);
  }

  @Override
  public TzRequisitionTemplate findByRequisitionTemplateId(UUID requisitionTemplateId) {
    Message errorMessage = new Message(TzRequisitionTemplateMessageKeys.ERROR_NOT_FOUND);
    return tzRequisitionTemplateRepository
            .findByRequisitionTemplateId(requisitionTemplateId)
            .orElseThrow(() -> new NotFoundException(errorMessage));
  }
}
