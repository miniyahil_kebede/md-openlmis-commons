/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;

import java.io.IOException;
import java.util.UUID;
import javax.inject.Inject;
import org.codehaus.jackson.map.ObjectMapper;
import org.openlmis.common.domain.ProcessingPeriod;
import org.openlmis.common.repository.ProcessingPeriodRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProcessingPeriodService {

  @Inject
  private PermissionService permissionService;

  @Inject
  private ProcessingPeriodRepository processingPeriodRepository;


  /**
   * Delete processing period of specified id.
   *
   * @param periodId id of the processing period to be deleted.
   */
  @Transactional
  public void deleteProcessingPeriod(UUID periodId) {
    permissionService.checkPermission(PermissionService.PROCESSING_SCHEDULES_MANAGE_RIGHT);
    processingPeriodRepository.verifyProcessingPeriodExists(periodId);

    Long reqCount = processingPeriodRepository.countProcessingPeriodRequisitions(periodId);
    if (reqCount > 0L) {
      throw new IllegalArgumentException(
          "This processing period  is already used in requisitions : " + periodId
      );
    }

    processingPeriodRepository.deleteProcessingPeriod(periodId);
  }

  /**
   * Delete program supported of specified id.
   *
   * @param program  id of the program supported to be deleted.
   * @param facility id of the program supported to be deleted.
   */
  @Transactional
  public void deleteProgramSupported(UUID program, UUID facility) {

    processingPeriodRepository.verifyProgramSupportedExists(program, facility);
    Long reqCount = processingPeriodRepository
        .countProgramSupportedRequisitions(program, facility);
    if (reqCount > 0L) {
      throw new IllegalArgumentException(
          "This program supported is already used in requisitions : " + program
      );
    }

    processingPeriodRepository.deleteProgramSupported(program, facility);
  }

  /**
   * Delete program supported of specified id.
   *
   * @param newPeriod id of the processing period to be deleted.
   */
  @Transactional
  public void saveProcessingPeriod(ProcessingPeriod newPeriod) throws IOException {
    ObjectMapper map = new ObjectMapper();

    newPeriod.setJsonData(map.writeValueAsString(
        newPeriod.getExtraData2()));
    processingPeriodRepository.saveProcessingPeriod(newPeriod);
  }

}
