/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service;


import static java.util.Collections.singletonList;
import static org.openlmis.common.i18n.MessageKeys.ERROR_IO;
import static org.openlmis.common.i18n.MessageKeys.ERROR_JASPER_FILE_FORMAT;
import static org.openlmis.common.i18n.MessageKeys.ERROR_JASPER_REPORT_CREATION_WITH_MESSAGE;
import static org.openlmis.common.i18n.MessageKeys.ERROR_JASPER_REPORT_FORMAT_UNKNOWN;
import static org.openlmis.common.i18n.MessageKeys.ERROR_JASPER_REPORT_GENERATION;
import static org.openlmis.common.i18n.MessageKeys.ERROR_REPORT_ID_NOT_FOUND;
import static org.openlmis.common.i18n.MessageKeys.NOT_FOUND;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.openlmis.common.domain.JasperExporter;
import org.openlmis.common.domain.JasperPdfExporter;
import org.openlmis.common.domain.JasperTemplate;
import org.openlmis.common.dto.RequisitionReportDto;
import org.openlmis.common.dto.RequisitionTemplateColumnDto;
import org.openlmis.common.dto.RequisitionTemplateDto;
import org.openlmis.common.dto.StockCardDto;
import org.openlmis.common.dto.StockCardSummaryDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.exception.ContentNotFoundMessageException;
import org.openlmis.common.exception.JasperReportViewException;
import org.openlmis.common.exception.ReportingException;
import org.openlmis.common.service.referencedata.LotReferenceDataService;
import org.openlmis.common.service.referencedata.OrderableReferenceDataService;
import org.openlmis.common.service.referencedata.StockCardReferenceDataService;
import org.openlmis.common.service.referencedata.StockCardSummariesReferenceDataService;
import org.openlmis.common.util.Message;
import org.openlmis.common.util.ReportUtils;
import org.openlmis.common.web.RequisitionReportDtoBuilder;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

/**
 * JasperReportsViewService class.
 */
@Service
public class JasperReportsViewService {

  private static final String REQUISITION_LINE_REPORT_DIR =
          "/jasperTemplates/requisitionLines.jrxml";
  static final String CARD_SUMMARY_REPORT_URL = "/jasperTemplates/stockCardSummary.jrxml";
  static final String CARD_REPORT_URL = "/jasperTemplates/stockCard.jrxml";

  private static final String REQUISITION_REPORT_DIR = "/jasperTemplates/requisition.jrxml";

  static final String PARAM_DATASOURCE = "datasource";
  private static final String DATASOURCE = "datasource";
  @Autowired
  private StockCardSummariesReferenceDataService stockCardSummariesService;

  @Autowired
  private StockCardReferenceDataService stockCardReferenceDataService;

  @Autowired
  private OrderableReferenceDataService orderableReferenceDataService;

  @Autowired
  private LotReferenceDataService lotReferenceDataService;


  @Autowired
  private RequisitionReportDtoBuilder requisitionReportDtoBuilder;

  @Autowired
  private DataSource replicationDataSource;
  @Value("${groupingSeparator}")
  private String groupingSeparator;

  @Value("${groupingSize}")
  private String groupingSize;
  @Value("${time.zoneId}")
  private String timeZoneId;

  @Value("${dateTimeFormat}")
  private String dateTimeFormat;

  @Value("${dateFormat}")
  private String dateFormat;

  @Value("${defaultLocale}")
  private String defaultLocale;

  @Value("${currencyLocale}")
  private String currencyLocale;

  private static final String templatePath = "/jasperTemplates/stockOutNotification.jrxml";

  private static final String msdLogo = "images/msd-logo.jpeg";

  private static final String msdSignature = "images/msd-signature.png";

  /**
   * Generate a report based on the Jasper template.
   * Create compiled report from bytes from Template entity, and use compiled report to fill in data
   * and export to desired format.
   *
   * @param jasperTemplate template that will be used to generate a report
   * @param params         map of parameters
   *
   * @return data of generated report
   */
  public byte[] generateReport(JasperTemplate jasperTemplate,
                               Map<String, Object> params)
      throws ReportingException {

    JasperReport jasperReport = getReportFromTemplateData(jasperTemplate);

    try {
      JasperPrint jasperPrint;
      if (params.containsKey(PARAM_DATASOURCE)) {
        jasperPrint = JasperFillManager.fillReport(jasperReport, params,
            new JRBeanCollectionDataSource((List) params.get(PARAM_DATASOURCE)));
      } else {
        try (Connection connection = replicationDataSource.getConnection()) {
          jasperPrint = JasperFillManager.fillReport(jasperReport, params,
              connection);
        }
      }

      return prepareReport(jasperPrint, params);
    } catch (Exception e) {
      throw new ReportingException(e, ERROR_JASPER_REPORT_CREATION_WITH_MESSAGE,
          e.getMessage());
    }
  }

  /**
   * Get (compiled) Jasper report from Jasper template.
   *
   * @param jasperTemplate template
   *
   * @return Jasper report
   */
  private JasperReport getReportFromTemplateData(JasperTemplate jasperTemplate)
      throws ReportingException {

    try (ObjectInputStream inputStream =
             new ObjectInputStream(new ByteArrayInputStream(jasperTemplate.getData()))) {

      return (JasperReport) inputStream.readObject();
    } catch (IOException ex) {
      throw new ReportingException(ex, ERROR_IO, ex.getMessage());
    } catch (ClassNotFoundException ex) {
      throw new ReportingException(ex, NOT_FOUND, JasperReport.class.getName());
    }
  }

  private byte[] prepareReport(JasperPrint jasperPrint, Map<String, Object> params)
      throws JRException {
    if (null != params.get("format")) {
      return getJasperExporter((String) params.get("format"), jasperPrint).exportReport();
    }

    return getJasperExporter("pdf", jasperPrint).exportReport();
  }

  private JasperExporter getJasperExporter(String keyParam, JasperPrint jasperPrint) {
    if ("pdf".equals(keyParam)) {
      return new JasperPdfExporter(jasperPrint);
    }
    throw new IllegalArgumentException(keyParam);
  }


  /**
   * Generate a report based on the Jasper template.
   * Create compiled report from bytes from Template entity, and use compiled report to fill in data
   * and export to pdf format.
   *
   * @param params map of parameters
   */
  public void generateNotification(Map<String, Object> params,
                                   HttpServletResponse response,
                                   Profiler profiler)
      throws ReportingException {

    try {
      ClassLoader classLoader = getClass().getClassLoader();

      params.put("msdLogo", String.valueOf(classLoader.getResource(msdLogo)));

      params.put("signature", String.valueOf(classLoader.getResource(msdSignature)));

      profiler.start("GENERATE JASPER REPORT");

      response.setContentType(MediaType.APPLICATION_PDF_VALUE);
      response.setHeader("Content-disposition",
          "inline; filename=stock_out_notification_report.pdf");

      JRBeanCollectionDataSource source =
          new JRBeanCollectionDataSource((List) params.get(PARAM_DATASOURCE));

      JasperPrint jasperPrint = JasperFillManager
          .fillReport(loadTemplate(profiler), params, source);

      profiler.start("EXPORT TO PDF");
      JasperExportManager.exportReportToPdfStream(jasperPrint,
          response.getOutputStream());

      profiler.stop();
    } catch (JRException | IOException e) {

      throw new ReportingException(e, ERROR_IO, e.getMessage());

    }
  }

  /**
   * load the template.
   *
   * @return the compiled jasper report
   */
  private JasperReport loadTemplate(Profiler profiler) throws JRException {

    profiler.start("GET_JRXML_FILE");
    InputStream stream = this.getClass().getResourceAsStream(templatePath);

    return JasperCompileManager.compileReport(stream);

  }

  /**
   * Get (compiled) Jasper report from Jasper template.
   *
   */
  public void getRequisitionJasperReportView(
          RequisitionDto requisition,
          OutputStream outputStream) throws JasperReportViewException, JRException {
    RequisitionReportDto reportDto = requisitionReportDtoBuilder.build(requisition);
    RequisitionTemplateDto template = requisition.getTemplate();
    Map<String, Object> params = ReportUtils.createParametersMap();
    params.put("subreport", createCustomizedRequisitionLineSubreport(template));
    params.put(DATASOURCE, singletonList(reportDto));
    params.put("programId", requisition.getProgram().getId());
    params.put("template", template);
    DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
    decimalFormatSymbols.setGroupingSeparator(groupingSeparator.charAt(0));
    DecimalFormat decimalFormat = new DecimalFormat("", decimalFormatSymbols);
    decimalFormat.setGroupingSize(Integer.parseInt(groupingSize));

    params.put("dateTimeFormat", dateTimeFormat);
    params.put("dateFormat", dateFormat);
    params.put("timeZoneId", timeZoneId);

    params.put("decimalFormat", decimalFormat);
    params.put("currencyDecimalFormat",
            NumberFormat.getCurrencyInstance(getLocaleFromService()));

    InputStream stream = this.getClass().getResourceAsStream(REQUISITION_REPORT_DIR);


    JasperReport jasperReport = JasperCompileManager.compileReport(stream);


    JasperPrint  jasperPrint = fillJasperReport(jasperReport, params,
            new JRBeanCollectionDataSource((List) params.get(DATASOURCE)));

    JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
  }

  JasperPrint fillJasperReport(JasperReport jasperReport, Map<String, Object> parameters,
                               JRDataSource dataSource) throws JRException {
    return JasperFillManager.fillReport(jasperReport, parameters, dataSource);
  }

  protected Locale getLocaleFromService() {
    return new Locale(defaultLocale, currencyLocale);
  }

  private JasperDesign createCustomizedRequisitionLineSubreport(RequisitionTemplateDto template)
          throws JasperReportViewException {
    try (InputStream inputStream = getClass().getResourceAsStream(REQUISITION_LINE_REPORT_DIR)) {
      JasperDesign design = JRXmlLoader.load(inputStream);
      JRBand detail = design.getDetailSection().getBands()[0];
      JRBand header = design.getColumnHeader();

      Map<String, RequisitionTemplateColumnDto> columns =
              ReportUtils.getSortedTemplateColumnsForPrint(template.getColumnsMap());

      ReportUtils.customizeBandWithTemplateFields(detail, columns, design.getPageWidth(), 9);
      ReportUtils.customizeBandWithTemplateFields(header, columns, design.getPageWidth(), 9);

      return design;
    } catch (IOException err) {
      throw new JasperReportViewException(err, ERROR_IO, err.getMessage());
    } catch (JRException err) {
      throw new JasperReportViewException(err, ERROR_JASPER_FILE_FORMAT, err.getMessage());
    }
  }


  /**
   * Create Jasper Report View.
   * Create Jasper Report (".jasper" file) from bytes from Template entity.
   * Set 'Jasper' exporter parameters, JDBC data source, web application context, url to file.
   *
   * @param jasperTemplate template that will be used to create a view
   * @param params  map of parameters
   * @return created jasper view.
   * @throws JasperReportViewException if there will be any problem with creating the view.
   */
  public byte[] getJasperReportsView(JasperTemplate jasperTemplate,
                                     Map<String, Object> params) throws JasperReportViewException {

    try {
      try (Connection connection = replicationDataSource.getConnection()) {
        ObjectInputStream inputStream = new ObjectInputStream(
                new ByteArrayInputStream(jasperTemplate.getData()));

        JasperPrint jasperPrint = JasperFillManager
                .fillReport((JasperReport) inputStream.readObject(), params, connection);

        return prepareReport(jasperPrint, params);
      }
    } catch (IllegalArgumentException iae) {
      throw new JasperReportViewException(iae,
              ERROR_JASPER_REPORT_FORMAT_UNKNOWN, iae.getMessage());
    } catch (Exception e) {
      throw new JasperReportViewException(e, ERROR_JASPER_REPORT_GENERATION);
    }
  }


  /**
   * Generate stock card summary report in PDF format.
   *
   * @param program  program id
   * @param facility facility id
   */
  public void generateStockCardSummariesReport(UUID program, UUID facility,
                                               OutputStream outputStream) throws JRException {
    List<StockCardSummaryDto> cardSummaries = stockCardSummariesService
            .findStockCardsSummaries(program, facility);
    Set cardIds = new HashSet();
    for (StockCardSummaryDto stockCardSummary : cardSummaries) {
      cardIds.addAll(stockCardSummary.getCanFulfillForMe()
              .stream()
              .map(c -> c.getStockCard().getId())
              .collect(Collectors.toList()));
    }
    List<StockCardDto> cards = stockCardReferenceDataService.findByIds(cardIds);
    cards.stream()
            .forEach(c -> {
              c.setOrderable(orderableReferenceDataService.findById(c.getOrderableId()));
              if (c.getLotId() != null) {
                c.setLot(lotReferenceDataService.findById(c.getLotId()));
              }
            });

    StockCardDto firstCard = cards.get(0);
    Map<String, Object> params =  ReportUtils.createParametersMap();
    params.put("stockCardSummaries", cards);

    params.put("program", firstCard.getProgram());
    params.put("facility", firstCard.getFacility());
    //right now, each report can only be about one program, one facility
    //in the future we may want to support one report for multiple programs
    params.put("showProgram", getCount(cards, card -> card.getProgram().getId().toString()) > 1);
    params.put("showFacility", getCount(cards, card -> card.getFacility().getId().toString()) > 1);
    params.put("showLot", cards.stream().anyMatch(card -> card.getLotId() != null));
    params.put("dateFormat", dateFormat);
    params.put("dateTimeFormat", dateTimeFormat);
    params.put("decimalFormat", createDecimalFormat());

    InputStream stream = this.getClass().getResourceAsStream(CARD_SUMMARY_REPORT_URL);


    JasperReport jasperReport = JasperCompileManager.compileReport(stream);


    JasperPrint  jasperPrint = fillJasperReport(jasperReport, params,
            new JREmptyDataSource());

    JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
  }

  /**
   * Generate stock card report in PDF format.
   *
   * @param stockCardId stock card id
   */
  public void generateStockCardReport(UUID stockCardId,
                                        OutputStream outputStream) throws JRException {
    StockCardDto stockCardDto = stockCardReferenceDataService.findById(stockCardId);
    if (stockCardDto == null) {
      throw new ContentNotFoundMessageException(new Message(ERROR_REPORT_ID_NOT_FOUND));
    }

    Collections.reverse(stockCardDto.getLineItems());
    Map<String, Object> params = ReportUtils.createParametersMap();
    params.put(PARAM_DATASOURCE, singletonList(stockCardDto));
    params.put("hasLot", stockCardDto.hasLot());
    params.put("dateFormat", dateFormat);
    params.put("decimalFormat", createDecimalFormat());

    InputStream stream = this.getClass().getResourceAsStream(CARD_REPORT_URL);


    JasperReport jasperReport = JasperCompileManager.compileReport(stream);


    JasperPrint  jasperPrint = fillJasperReport(jasperReport, params,
            new JRBeanCollectionDataSource((List<StockCardDto>) params.get(PARAM_DATASOURCE)));

    JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
  }

  private long getCount(List<StockCardDto> stockCards, Function<StockCardDto, String> mapper) {
    return stockCards.stream().map(mapper).distinct().count();
  }

  private DecimalFormat createDecimalFormat() {
    DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
    decimalFormatSymbols.setGroupingSeparator(groupingSeparator.charAt(0));
    DecimalFormat decimalFormat = new DecimalFormat("", decimalFormatSymbols);
    decimalFormat.setGroupingSize(Integer.parseInt(groupingSize));
    return decimalFormat;
  }
}