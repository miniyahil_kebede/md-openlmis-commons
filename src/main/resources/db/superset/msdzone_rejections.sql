SELECT 
(SELECT z.name FROM public.kafka_geographic_zones z WHERE z.id = ft.regionParentId) as msdZone,
count(regionParentId) as count,program,period,schedule,(SELECT DATE_PART('year', rdate::DATE)) AS year

FROM
(SELECT p.name as program,pp.name as period,psch.name as schedule,
r.createddate as rdate,
(SELECT dt.regionParentId
FROM (SELECT 
(SELECT z.parentid FROM public.kafka_geographic_zones z WHERE z.id = gz.parentid) as regionParentId
FROM public.kafka_geographic_zones gz
WHERE gz.id =f.geographiczoneid) as dt) as regionParentId


FROM public.kafka_rejections r 
INNER JOIN public.kafka_rejection_reasons rr ON r.rejectionreasonid=rr.id 
INNER JOIN public.kafka_status_changes sc ON r.statuschangeid=sc.id::TEXT
INNER JOIN public.kafka_requisitions req ON sc.requisitionid=req.id
INNER JOIN public.kafka_facilities f ON f.id=req.facilityid
INNER JOIN public.kafka_programs p ON req.programid=p.id
INNER JOIN public.kafka_processing_periods pp ON req.processingperiodid=pp.id
INNER JOIN public.kafka_processing_schedules psch ON pp.processingscheduleid=psch.id) as ft
GROUP BY ft.regionparentid, ft.program, ft.period,ft.schedule, ft.rdate;

