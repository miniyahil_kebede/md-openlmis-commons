CREATE TABLE IF NOT EXISTS common.geographic_zone_geojson (
                                                                     id uuid PRIMARY KEY,
                                                                     zoneid uuid,
                                                                     geojsonid integer,
                                                                     geometry text
);

-- Create an index on the id column if the table was just created
CREATE INDEX IF NOT EXISTS geographic_zone_geojson_id_idx ON common.geographic_zone_geojson (id);
