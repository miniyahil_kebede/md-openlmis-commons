DO $$
BEGIN

    -- Check if the schema exists
    IF EXISTS(SELECT * FROM   information_schema.schemata WHERE schema_name = 'auth') THEN
        CREATE TABLE IF NOT EXISTS auth.auth_users (
                                               id uuid NOT NULL,
                                               email character varying(255) NOT NULL,
                                                enabled boolean,
                                                password character varying(255),
                                                referencedatauserid uuid NOT NULL,
                                                role character varying(255) NOT NULL,
                                                username character varying(255) NOT NULL
                                                );

        CREATE TABLE IF NOT EXISTS auth.password_reset_tokens (
                                                  id uuid NOT NULL,
                                                  expirydate timestamp with time zone NOT NULL,
                                                  userid uuid NOT NULL
                                                    );
END IF;
END $$;