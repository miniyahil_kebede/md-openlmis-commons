--
-- Name: application_interfaces; Type: TABLE; Schema: common; Owner: postgres; Tablespace:
--

CREATE TABLE application_interfaces
(
    id           uuid NOT NULL,
    createdDate  timestamptz,
    modifiedDate timestamptz,
    name         text NOT NULL,
    active       boolean DEFAULT TRUE,

    CONSTRAINT application_interfaces_pkey PRIMARY KEY (id)
);


--
-- Name: interface_datasets; Type: TABLE; Schema: common; Owner: postgres; Tablespace:
--

CREATE TABLE interface_datasets
(
    id    uuid NOT NULL,
    name  text NOT NULL,
    value text NOT NULL,
    interfaceId uuid NOT NULL,
    CONSTRAINT interface_datasets_pkey PRIMARY KEY (id),
    CONSTRAINT fkey_application_interfaces FOREIGN KEY (interfaceId) REFERENCES application_interfaces (id)
);
