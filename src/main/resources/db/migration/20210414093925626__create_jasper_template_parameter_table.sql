--
-- Name: jasper_templates; Type: TABLE; Schema: common; Owner: postgres; Tablespace:
--

CREATE TABLE jasper_templates (
                                  id uuid NOT NULL,
                                  data bytea,
                                  description text,
                                  name text NOT NULL,
                                  type text
);

--
-- Name: jasper_templates_pkey; Type: CONSTRAINT; Schema: common; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY jasper_templates
    ADD CONSTRAINT jasper_templates_pkey PRIMARY KEY (id);


--
-- Name: uk_5878s5vb2v4y53vun95nrdvgw; Type: CONSTRAINT; Schema: requisition; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY jasper_templates
    ADD CONSTRAINT uk_5878s5vb2v4y53vun95nrdvgw UNIQUE (name);

ALTER TABLE common.jasper_templates ADD isDisplayed boolean;

--
-- Name: jasper_template_parameters; Type: TABLE; Schema: common; Owner: postgres; Tablespace:
--

CREATE TABLE jasper_template_parameters (
                                     id uuid NOT NULL,
                                     datatype text,
                                     defaultvalue text,
                                     description text,
                                     displayname text,
                                     name text,
                                     selectsql text,
                                     templateid uuid NOT NULL
);


--
-- Name: jasper_template_parameters_pkey; Type: CONSTRAINT; Schema: common; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY jasper_template_parameters
    ADD CONSTRAINT jasper_template_parameters_pkey PRIMARY KEY (id);


ALTER TABLE common.jasper_template_parameters RENAME COLUMN selectSql to selectExpression;
ALTER TABLE common.jasper_template_parameters ADD selectProperty text;
ALTER TABLE common.jasper_template_parameters ADD displayProperty text;
ALTER TABLE common.jasper_template_parameters ADD required boolean;

ALTER TABLE common.jasper_template_parameters ADD selectMethod text;
ALTER TABLE common.jasper_template_parameters ADD selectBody text;