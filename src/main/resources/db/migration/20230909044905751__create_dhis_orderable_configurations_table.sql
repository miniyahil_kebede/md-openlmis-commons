CREATE TABLE dhis_orderable_configurations
(
    id uuid NOT NULL,
    value text not null,
    orderableId uuid NOT NULL,
    CONSTRAINT dhis_orderable_configurations_pkey PRIMARY KEY (id)
);