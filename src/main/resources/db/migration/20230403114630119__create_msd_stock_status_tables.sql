CREATE TABLE msd_daily_stock_status
(
    id uuid NOT NULL,
    modifiedDate         timestamp with time zone,
    createdDate          timestamp with time zone,
    ilnumber     text NOT NULL
);

ALTER TABLE ONLY msd_daily_stock_status
    ADD CONSTRAINT msd_daily_stock_status_pkey PRIMARY KEY (id);

CREATE TABLE msd_daily_stock_status_value
(
    id uuid NOT NULL,
    plant     text NOT NULL,
    partnumber     text NOT NULL,
    unitofmeasure     text NOT NULL,
    partdescription     text NOT NULL,
    date     text NOT NULL,
    monthofstock     integer NOT NULL,
    onhandquantity     text NOT NULL
);

ALTER TABLE ONLY msd_daily_stock_status_value
    ADD CONSTRAINT msd_daily_stock_status_value_pkey PRIMARY KEY (id);

CREATE TABLE msd_daily_stock_status_values
(
    statusid uuid NOT NULL,
    valueid uuid NOT NULL
);

ALTER TABLE ONLY msd_daily_stock_status_values
    ADD CONSTRAINT fk_msd_daily_stock_status
    foreign key (statusid) references msd_daily_stock_status (id);


ALTER TABLE ONLY msd_daily_stock_status_values
    ADD CONSTRAINT fk_msd_daily_stock_status_value
    foreign key (valueid) references msd_daily_stock_status_value (id);